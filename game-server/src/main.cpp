#include <iostream>

#include <jwt-cpp/jwt.h>

#include <keys.hpp>

#include "microservice.hpp"
#include <usr_interrupt_handler.hpp>
#include <runtime_utils.hpp>
#include "endpoint.hpp"

constexpr auto QUEUE_NAME = "test_queue";

int main(int, char **)
{
    std::cout << "Game server starting..." << std::endl;
    std::cout << "Public key:" << std::endl
              << JWT_PUBLIC_KEY << std::endl;

    InterruptHandler::hookSIGINT();

    MicroserviceController server;
    server.setEndpoint("http://host_auto_ip4:6502/v1/game/api");

    try
    {
        // wait for server initialization...
        server.accept().wait();
        std::cout << "Game Microservice now listening for requests at: " << server.endpoint() << '\n';

        InterruptHandler::waitForUserInterrupt();

        server.shutdown().wait();
    }
    catch (std::exception &e)
    {
        std::cerr << "Microservice exception: " << e.what() << std::endl;
    }
    catch (...)
    {
        RuntimeUtils::printStackTrace();
    }

    return 0;
}
