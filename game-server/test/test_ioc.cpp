#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <string_view>
#include <string>
#include <memory>
#include <thread>

#include "mocks.hpp"
#include "ioc.hpp"

using namespace ::testing;
using namespace std::literals::string_view_literals;

TEST(IoC, RegisterUnregister)
{
    constexpr MoveData START_POSITION{12, 5};
    constexpr MoveData VELOCITY{-7, 3};
    constexpr MoveData STOP_POSITION{5, 8};

    MovableMock movableMock{};
    EXPECT_CALL(movableMock, GetPosition).WillOnce(Return(START_POSITION));
    EXPECT_CALL(movableMock, GetVelocity).WillOnce(Return(VELOCITY));
    EXPECT_CALL(movableMock, SetPosition(STOP_POSITION));

    IoC::Resolve<std::shared_ptr<IoC::Register>>(
        "IoC.Register", "Move"sv,
        IoC::Factory{[](const std::vector<std::any> &args)
                     {
                         return std::make_shared<Move>(std::any_cast<IMovable *>(args.at(0)));
                     }})
        ->Execute();

    auto move{IoC::Resolve<std::shared_ptr<Move>>("Move", static_cast<IMovable *>(&movableMock))};
    if (move)
    {
        move->Execute();
    }

    IoC::Resolve<std::shared_ptr<IoC::Unregister>>(
        "IoC.Unregister", "Move"sv)
        ->Execute();

    ASSERT_THROW(std::ignore = IoC::Resolve<Move *>("Move", static_cast<IMovable *>(&movableMock)), IocException);
}

TEST(IoC, Scope)
{
    constexpr MoveData START_POSITION{12, 5};
    constexpr MoveData VELOCITY{-7, 3};
    constexpr MoveData STOP_POSITION{5, 8};
    constexpr std::string_view MY_SCOPE_ID{"MyScope"};

    MovableMock movableMock{};

    // Create and change scope
    IoC::Resolve<std::shared_ptr<IoC::NewScope>>("Scope.New", MY_SCOPE_ID)->Execute();
    IoC::Resolve<std::shared_ptr<IoC::SetScope>>("Scope.Current", MY_SCOPE_ID)->Execute();

    // Register factory in MyScope
    IoC::Resolve<std::shared_ptr<IoC::Register>>(
        "IoC.Register", "Move"sv,
        IoC::Factory{[](const std::vector<std::any> &args)
                     {
                         return std::make_shared<Move>(std::any_cast<IMovable *>(args.at(0)));
                     }})
        ->Execute();

    auto move = IoC::Resolve<std::shared_ptr<Move>>("Move", static_cast<IMovable *>(&movableMock));
    ASSERT_TRUE(move);
    EXPECT_CALL(movableMock, GetPosition).WillOnce(Return(START_POSITION));
    EXPECT_CALL(movableMock, GetVelocity).WillOnce(Return(VELOCITY));
    EXPECT_CALL(movableMock, SetPosition(STOP_POSITION));
    move->Execute();

    IoC::Resolve<std::shared_ptr<IoC::SetScope>>("Scope.Current", IoC::ROOT_SCOPE_ID)->Execute();
    ASSERT_THROW(IoC::Resolve<std::shared_ptr<Move>>("Move", static_cast<IMovable *>(&movableMock)), IocException);

    IoC::Resolve<std::shared_ptr<IoC::SetScope>>("Scope.Current", MY_SCOPE_ID)->Execute();
    std::shared_ptr<Move> move3{};
    EXPECT_NO_THROW(move3 = IoC::Resolve<std::shared_ptr<Move>>("Move", static_cast<IMovable *>(&movableMock)));
    ASSERT_TRUE(move3);
    EXPECT_CALL(movableMock, GetPosition).WillOnce(Return(START_POSITION));
    EXPECT_CALL(movableMock, GetVelocity).WillOnce(Return(VELOCITY));
    EXPECT_CALL(movableMock, SetPosition(STOP_POSITION));
    move3->Execute();
}

// Check what Register from other thread does not exist
void threadTestFunc2()
{
    MovableMock movableMock{};

    ASSERT_THROW(IoC::Resolve<std::shared_ptr<Move>>("Move", static_cast<IMovable *>(&movableMock)), IocException);
}

// Create new Register
void threadTestFunc1()
{
    constexpr MoveData START_POSITION{12, 5};
    constexpr MoveData VELOCITY{-7, 3};
    constexpr MoveData STOP_POSITION{5, 8};

    MovableMock movableMock{};

    IoC::Resolve<std::shared_ptr<IoC::Register>>(
        "IoC.Register", "Move"sv,
        IoC::Factory{[](const std::vector<std::any> &args)
                     {
                         return std::make_shared<Move>(std::any_cast<IMovable *>(args.at(0)));
                     }})
        ->Execute();

    auto move = IoC::Resolve<std::shared_ptr<Move>>("Move", static_cast<IMovable *>(&movableMock));
    ASSERT_TRUE(move);
    EXPECT_CALL(movableMock, GetPosition).WillOnce(Return(START_POSITION));
    EXPECT_CALL(movableMock, GetVelocity).WillOnce(Return(VELOCITY));
    EXPECT_CALL(movableMock, SetPosition(STOP_POSITION));
    move->Execute();

    std::thread testThread(threadTestFunc2);
    testThread.join();
}

TEST(IoC, Multithread)
{
    std::thread testThread(threadTestFunc1);
    testThread.join();
}
