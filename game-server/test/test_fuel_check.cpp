#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <stdexcept>

#include "fuel_check.hpp"
#include "mocks.hpp"

using namespace ::testing;

TEST(FuelCheck, Normal)
{
    constexpr int CONSUMPTION{5};
    constexpr int LEVEL{CONSUMPTION + 1};

    FuelCheckableMock mock{};
    FuelCheck fuelCheck{&mock};

    EXPECT_CALL(mock, GetConsumption).WillOnce(Return(CONSUMPTION));
    EXPECT_CALL(mock, GetLevel).WillOnce(Return(LEVEL));

    EXPECT_NO_THROW(fuelCheck.Execute());
}

TEST(FuelCheck, Low)
{
    constexpr int CONSUMPTION{5};
    constexpr int LEVEL{CONSUMPTION - 1};

    FuelCheckableMock mock{};
    FuelCheck fuelCheck{&mock};

    EXPECT_CALL(mock, GetConsumption).WillOnce(Return(CONSUMPTION));
    EXPECT_CALL(mock, GetLevel).WillOnce(Return(LEVEL));

    EXPECT_ANY_THROW(fuelCheck.Execute());
}
