#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <stdexcept>

#include "move.hpp"
#include "mocks.hpp"

using namespace ::testing;

ACTION_P(ThrowException, str)
{
    throw std::runtime_error{str};
}

TEST(Move, Normal)
{
    constexpr MoveData START_POSITION{12, 5};
    constexpr MoveData VELOCITY{-7, 3};
    constexpr MoveData STOP_POSITION{5, 8};

    MovableMock movableMock{};
    Move move{&movableMock};

    EXPECT_CALL(movableMock, GetPosition).WillOnce(Return(START_POSITION));
    EXPECT_CALL(movableMock, GetVelocity).WillOnce(Return(VELOCITY));
    EXPECT_CALL(movableMock, SetPosition(STOP_POSITION));

    EXPECT_NO_THROW(move.Execute());
}

TEST(Move, GetPositionError)
{
    MovableMock movableMock{};
    Move move{&movableMock};

    EXPECT_CALL(movableMock, GetPosition).WillOnce(ThrowException("Get position error"));

    EXPECT_ANY_THROW(move.Execute());
}

TEST(Move, VelocityError)
{
    MovableMock movableMock{};
    Move move{&movableMock};

    EXPECT_CALL(movableMock, GetVelocity).WillOnce(ThrowException("Velocity error"));

    EXPECT_ANY_THROW(move.Execute());
}

TEST(Move, SetPositionError)
{
    MovableMock movableMock{};
    Move move{&movableMock};

    EXPECT_CALL(movableMock, SetPosition).WillOnce(ThrowException("Set position error"));

    EXPECT_ANY_THROW(move.Execute());
}
