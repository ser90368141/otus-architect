#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <stdexcept>

#include "repeater.hpp"
#include "mocks.hpp"

using namespace ::testing;

ACTION(ThrowException)
{
    throw CommandException{};
}

TEST(Repeater, Normal)
{
    auto cmdMock = std::make_unique<CommandMock>();

    EXPECT_CALL(*cmdMock, Execute).Times(1);
    Repeater repeater{std::move(cmdMock)};

    EXPECT_NO_THROW(repeater.Execute());
}

TEST(Repeater, Exception)
{
    auto cmdMock = std::make_unique<CommandMock>();

    EXPECT_CALL(*cmdMock, Execute).WillOnce(ThrowException());
    Repeater repeater{std::move(cmdMock)};

    EXPECT_THROW(repeater.Execute(), CommandException);
}
