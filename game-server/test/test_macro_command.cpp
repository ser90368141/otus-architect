#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <stdexcept>

#include "macro_command.hpp"
#include "mocks.hpp"

using namespace ::testing;

ACTION(ThrowException)
{
    throw CommandException{};
}

TEST(MacroCommand, TwoCommands)
{
    auto mock1{std::make_shared<CommandMock>()};
    auto mock2{std::make_shared<CommandMock>()};
    MacroCommand macro(MacroCommand::CmdArray{mock1, mock2});

    EXPECT_CALL(*mock1, Execute).Times(1);
    EXPECT_CALL(*mock2, Execute).Times(1);

    EXPECT_NO_THROW(macro.Execute());
}

TEST(MacroCommand, ThreeCommands)
{
    auto mock1{std::make_shared<CommandMock>()};
    auto mock2{std::make_shared<CommandMock>()};
    auto mock3{std::make_shared<CommandMock>()};
    MacroCommand macro(MacroCommand::CmdArray{mock1, mock2, mock3});

    EXPECT_CALL(*mock1, Execute).Times(1);
    EXPECT_CALL(*mock2, Execute).Times(1);
    EXPECT_CALL(*mock3, Execute).Times(1);

    EXPECT_NO_THROW(macro.Execute());
}

TEST(MacroCommand, CommandException)
{
    auto mock1{std::make_shared<CommandMock>()};
    auto mock2{std::make_shared<CommandMock>()};
    auto mock3{std::make_shared<CommandMock>()};
    MacroCommand macro(MacroCommand::CmdArray{mock1, mock2, mock3});

    EXPECT_CALL(*mock1, Execute).Times(1);
    EXPECT_CALL(*mock2, Execute).WillOnce(ThrowException());
    EXPECT_CALL(*mock3, Execute).Times(0);

    EXPECT_THROW(macro.Execute(), CommandException);
}

TEST(MoveWithFuelCommand, FuelOk)
{
    auto fuelCheckMock{std::make_shared<CommandMock>()};
    auto moveMock{std::make_shared<CommandMock>()};
    auto fuelBurnMock{std::make_shared<CommandMock>()};

    MoveWithBurn moveWithFuel(fuelCheckMock, moveMock, fuelBurnMock);

    EXPECT_CALL(*fuelCheckMock, Execute).Times(1);
    EXPECT_CALL(*moveMock, Execute).Times(1);
    EXPECT_CALL(*fuelBurnMock, Execute).Times(1);

    EXPECT_NO_THROW(moveWithFuel.Execute());
}

TEST(MoveWithFuelCommand, FuelLowLevel)
{
    auto fuelCheckMock{std::make_shared<CommandMock>()};
    auto moveMock{std::make_shared<CommandMock>()};
    auto fuelBurnMock{std::make_shared<CommandMock>()};

    MoveWithBurn moveWithFuel(fuelCheckMock, moveMock, fuelBurnMock);

    EXPECT_CALL(*fuelCheckMock, Execute).WillOnce(ThrowException());
    EXPECT_CALL(*moveMock, Execute).Times(0);
    EXPECT_CALL(*fuelBurnMock, Execute).Times(0);

    EXPECT_THROW(moveWithFuel.Execute(), CommandException);
}

TEST(MoveWithFuelVelocityCommand, FuelOk)
{
    auto fuelCheckMock{std::make_shared<CommandMock>()};
    auto moveMock{std::make_shared<CommandMock>()};
    auto fuelBurnMock{std::make_shared<CommandMock>()};
    auto velocityChangeMock{std::make_shared<CommandMock>()};

    MoveWithBurnVelocityChange moveWithFuelVelocity(fuelCheckMock, moveMock, fuelBurnMock, velocityChangeMock);

    EXPECT_CALL(*fuelCheckMock, Execute).Times(1);
    EXPECT_CALL(*moveMock, Execute).Times(1);
    EXPECT_CALL(*fuelBurnMock, Execute).Times(1);
    EXPECT_CALL(*velocityChangeMock, Execute).Times(1);

    EXPECT_NO_THROW(moveWithFuelVelocity.Execute());
}

TEST(MoveWithFuelVelocityCommand, FuelLowLevel)
{
    auto fuelCheckMock{std::make_shared<CommandMock>()};
    auto moveMock{std::make_shared<CommandMock>()};
    auto fuelBurnMock{std::make_shared<CommandMock>()};
    auto velocityChangeMock{std::make_shared<CommandMock>()};

    MoveWithBurnVelocityChange moveWithFuelVelocity(fuelCheckMock, moveMock, fuelBurnMock, velocityChangeMock);

    EXPECT_CALL(*fuelCheckMock, Execute).WillOnce(ThrowException());
    EXPECT_CALL(*moveMock, Execute).Times(0);
    EXPECT_CALL(*fuelBurnMock, Execute).Times(0);
    EXPECT_CALL(*velocityChangeMock, Execute).Times(0);

    EXPECT_THROW(moveWithFuelVelocity.Execute(), CommandException);
}
