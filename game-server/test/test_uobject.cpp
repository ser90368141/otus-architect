#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <iostream>

#include "mocks.hpp"
#include "uobject.hpp"

using namespace ::testing;

TEST(UObject, Test)
{
    constexpr std::string_view KEY{"TestKey"};
    constexpr int VALUE{123};
    UObject obj{};
    
    obj.SetProperty(KEY, VALUE);
    EXPECT_EQ(std::any_cast<int>(obj.GetProperty(KEY)), VALUE);
}
