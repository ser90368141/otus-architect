#pragma once

#include <gmock/gmock.h>
#include <string_view>
#include <unordered_map>

#include "rotate.hpp"
#include "move.hpp"
#include "command.hpp"
#include "fuel_check.hpp"
#include "fuel_burn.hpp"
#include "velocity_change.hpp"
#include "log.hpp"
#include "queue.hpp"
#include "collision_check.hpp"

using namespace ::testing;

class CommandMock : public ICommand
{
public:
    MOCK_METHOD(void, Execute, (), (override));
};

class RotatableMock : public IRotatable
{
public:
    MOCK_METHOD(int, GetDirection, (), (override));
    MOCK_METHOD(int, GetAngularVelocity, (), (override));
    MOCK_METHOD(void, SetDirection, (int), (override));
    MOCK_METHOD(int, GetDirectionsNumber, (), (override));
};

class MovableMock : public IMovable
{
public:
    MOCK_METHOD(MoveData, GetPosition, (), (override));
    MOCK_METHOD(void, SetPosition, (MoveData), (override));
    MOCK_METHOD(MoveData, GetVelocity, (), (override));
};

class FuelCheckableMock : public IFuelCheckable
{
public:
    MOCK_METHOD(int, GetConsumption, (), (override));
    MOCK_METHOD(int, GetLevel, (), (override));
};

class FuelBurnableMock : public IFuelBurnable
{
public:
    MOCK_METHOD(int, GetConsumption, (), (override));
    MOCK_METHOD(int, GetLevel, (), (override));
    MOCK_METHOD(void, SetLevel, (int), (override));
};

class VelocityChangeableMock : public IVelocityChangeable
{
public:
    MOCK_METHOD(void, SetVelocity, (MoveData), (override));
    MOCK_METHOD(MoveData, GetVelocity, (), (override));
    MOCK_METHOD(int, GetDirection, (), (override));
    MOCK_METHOD(int, GetDirectionsNumber, (), (override));
};

class LogableMock : public ILogable
{
public:
    MOCK_METHOD(void, Write, (), (override));
};

template <typename T>
class QueueMock : public IQueue<T>
{
public:
    MOCK_METHOD(void, Push, (std::shared_ptr<T>), (override));
    MOCK_METHOD(void, PushUnsave, (std::shared_ptr<T>), (override));
    MOCK_METHOD(std::shared_ptr<T>, Pop, (), (override));
    MOCK_METHOD(std::size_t, GetSize, (), (override));
};

class PositionCheckableMock : public IPositionCheckable
{
public:
    MOCK_METHOD(MoveData, GetPosition, (), (override));
};

class NeighborhoodFindableMock : public INeighborhoodFindable
{
public:
    MOCK_METHOD(MoveData, GetPosition, (Id), (override));
    MOCK_METHOD(Id, GetObjId, (), (override));
    MOCK_METHOD(IQueue<ICommand> *, GetQueue, (), (override));
};

class TestMock
{
public:
    MOCK_METHOD(void, Test, (), ());
};
