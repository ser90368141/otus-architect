#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <stdexcept>

#include "velocity_change.hpp"
#include "mocks.hpp"

using namespace ::testing;

TEST(VelocityChange, Test1)
{
    constexpr MoveData START_VELOCITY{8, 8};
    constexpr int DIRECTION{4};
    constexpr int DIRECTIONS_NUMBER{8};
    constexpr MoveData STOP_VELOCITY{0, 8};

    VelocityChangeableMock mock{};
    VelocityChange velocityChange{&mock};

    EXPECT_CALL(mock, GetVelocity).WillOnce(Return(START_VELOCITY));
    EXPECT_CALL(mock, GetDirection).WillOnce(Return(DIRECTION));
    EXPECT_CALL(mock, GetDirectionsNumber).WillOnce(Return(DIRECTIONS_NUMBER));
    EXPECT_CALL(mock, SetVelocity(STOP_VELOCITY));

    EXPECT_NO_THROW(velocityChange.Execute());
}

TEST(VelocityChange, Test2)
{
    constexpr MoveData START_VELOCITY{-8, 8};
    constexpr int DIRECTION{4};
    constexpr int DIRECTIONS_NUMBER{8};
    constexpr MoveData STOP_VELOCITY{0, 8};

    VelocityChangeableMock mock{};
    VelocityChange velocityChange{&mock};

    EXPECT_CALL(mock, GetVelocity).WillOnce(Return(START_VELOCITY));
    EXPECT_CALL(mock, GetDirection).WillOnce(Return(DIRECTION));
    EXPECT_CALL(mock, GetDirectionsNumber).WillOnce(Return(DIRECTIONS_NUMBER));
    EXPECT_CALL(mock, SetVelocity(STOP_VELOCITY));

    EXPECT_NO_THROW(velocityChange.Execute());
}

TEST(VelocityChange, Test3)
{
    constexpr MoveData START_VELOCITY{8, 8};
    constexpr int DIRECTION{0};
    constexpr int DIRECTIONS_NUMBER{8};
    constexpr MoveData STOP_VELOCITY{16, 8};

    VelocityChangeableMock mock{};
    VelocityChange velocityChange{&mock};

    EXPECT_CALL(mock, GetVelocity).WillOnce(Return(START_VELOCITY));
    EXPECT_CALL(mock, GetDirection).WillOnce(Return(DIRECTION));
    EXPECT_CALL(mock, GetDirectionsNumber).WillOnce(Return(DIRECTIONS_NUMBER));
    EXPECT_CALL(mock, SetVelocity(STOP_VELOCITY));

    EXPECT_NO_THROW(velocityChange.Execute());
}

TEST(VelocityChange, Test4)
{
    constexpr MoveData START_VELOCITY{0, 0};

    VelocityChangeableMock mock{};
    VelocityChange velocityChange{&mock};

    EXPECT_CALL(mock, GetVelocity).WillOnce(Return(START_VELOCITY));
    EXPECT_CALL(mock, GetDirection).Times(0);
    EXPECT_CALL(mock, GetDirectionsNumber).Times(0);
    EXPECT_CALL(mock, SetVelocity).Times(0);

    EXPECT_NO_THROW(velocityChange.Execute());
}