#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <stdexcept>

#include "collision_check.hpp"
#include "mocks.hpp"

using namespace ::testing;

TEST(PositionCheck, PositionCheckNormal)
{
    constexpr MoveData POSITION_1{12, 5};
    constexpr MoveData POSITION_2{POSITION_1.x + 1, POSITION_1.y - 1};

    PositionCheckableMock positionCheckableMock{};
    PositionCheck positionCheck{&positionCheckableMock, POSITION_2};

    EXPECT_CALL(positionCheckableMock, GetPosition).WillOnce(Return(POSITION_1));

    EXPECT_NO_THROW(positionCheck.Execute());
}

TEST(PositionCheck, PositionCheckError)
{
    constexpr MoveData POSITION_1{12, 5};
    constexpr MoveData POSITION_2{POSITION_1.x, POSITION_1.y};

    PositionCheckableMock positionCheckableMock{};
    PositionCheck positionCheck{&positionCheckableMock, POSITION_2};

    EXPECT_CALL(positionCheckableMock, GetPosition).WillOnce(Return(POSITION_1));

    EXPECT_ANY_THROW(positionCheck.Execute());
}

TEST(PositionCheck, FindNeighborhood)
{
    // Map (2x2) with neighborhood size = 4
    // 0  1
    // 2  3
    NeighborhoodMap map{};
    map.emplace(0, Neighborhood{0, 0, 4, {}});
    map.emplace(1, Neighborhood{4, 0, 4, {}});

    map.emplace(2, Neighborhood{0, 4, 4, {}});
    map.emplace(3, Neighborhood{4, 4, 4, {}});

    NeighborhoodMap *mapAddr{&map};
    Neighborhood *currentObjNeig{&map.at(0)};
    Neighborhood *newObjNeig{&map.at(3)};
    constexpr MoveData POS{6, 5};
    constexpr std::uint64_t OBJ_ID{556677};

    PositionCheckableMock positionCheckableMock{};
    NeighborhoodFindableMock neighborhoodFindableMock{};
    NeighborhoodFind neighborhoodFind{&neighborhoodFindableMock, &positionCheckableMock, &map};

    // Check adapter functions call
    EXPECT_CALL(positionCheckableMock, GetPosition()).WillOnce(Return(POS));
    EXPECT_CALL(neighborhoodFindableMock, GetObjId()).WillOnce(Return(OBJ_ID));

    EXPECT_NO_THROW(neighborhoodFind.Execute());

    // Check that new neighborhood have new object
    EXPECT_TRUE(newObjNeig->objects.find(OBJ_ID) != newObjNeig->objects.end());
}

TEST(PositionCheck, FindWithoutCollision)
{
    constexpr std::uint64_t OBJ1_ID{12345};
    constexpr MoveData POS1{6, 5};
    constexpr auto MAP_IDX1{0};

    constexpr std::uint64_t OBJ2_ID{67891};
    constexpr MoveData POS2{6, 6};
    constexpr auto MAP_IDX2{3};
    // Map (2x2) with neighborhood size = 4
    // 0  1
    // 2  3
    NeighborhoodMap map{};
    map.emplace(0, Neighborhood{0, 0, 4, {}});
    map.emplace(1, Neighborhood{4, 0, 4, {}});

    map.emplace(2, Neighborhood{0, 4, 4, {}});
    map.emplace(3, Neighborhood{4, 4, 4, {}});
    map.at(MAP_IDX1).objects.insert(OBJ1_ID);
    map.at(MAP_IDX2).objects.insert(OBJ2_ID);

    NeighborhoodMap *mapAddr{&map};
    Neighborhood *currentObjNeig{&map.at(MAP_IDX1)};
    Neighborhood *newObjNeig{&map.at(MAP_IDX2)};

    PositionCheckableMock positionCheckableMock{};
    NeighborhoodFindableMock neighborhoodFindableMock{};
    QueueMock<ICommand> queueMock{};
    NeighborhoodFind neighborhoodFind{&neighborhoodFindableMock, &positionCheckableMock, &map};
    std::shared_ptr<ICommand> macroCommand;

    // Check adapter functions call
    EXPECT_CALL(positionCheckableMock, GetPosition()).WillOnce(Return(POS1));
    EXPECT_CALL(neighborhoodFindableMock, GetObjId()).WillOnce(Return(OBJ1_ID));
    EXPECT_CALL(neighborhoodFindableMock, GetPosition(OBJ2_ID)).WillOnce(Return(POS2));
    EXPECT_CALL(neighborhoodFindableMock, GetQueue()).WillOnce(Return(&queueMock));
    EXPECT_CALL(queueMock, Push(_)).WillOnce(SaveArg<0>(&macroCommand));

    EXPECT_NO_THROW(neighborhoodFind.Execute());

    // Check that new neighborhood have new object
    EXPECT_TRUE(newObjNeig->objects.find(OBJ1_ID) != newObjNeig->objects.end());
    EXPECT_TRUE(typeid(*macroCommand) == typeid(MacroCommand));

    EXPECT_CALL(positionCheckableMock, GetPosition).WillOnce(Return(POS1));

    // Check collision exception
    EXPECT_NO_THROW(macroCommand->Execute());
}

TEST(PositionCheck, FindWithCollision)
{
    constexpr std::uint64_t OBJ1_ID{12345};
    constexpr MoveData POS1{6, 5};
    constexpr auto MAP_IDX1{0};

    constexpr std::uint64_t OBJ2_ID{67891};
    constexpr MoveData POS2{POS1};
    constexpr auto MAP_IDX2{3};
    // Map (2x2) with neighborhood size = 4
    // 0  1
    // 2  3
    NeighborhoodMap map{};
    map.emplace(0, Neighborhood{0, 0, 4, {}});
    map.emplace(1, Neighborhood{4, 0, 4, {}});

    map.emplace(2, Neighborhood{0, 4, 4, {}});
    map.emplace(3, Neighborhood{4, 4, 4, {}});
    map.at(MAP_IDX1).objects.insert(OBJ1_ID);
    map.at(MAP_IDX2).objects.insert(OBJ2_ID);

    NeighborhoodMap *mapAddr{&map};
    Neighborhood *currentObjNeig{&map.at(MAP_IDX1)};
    Neighborhood *newObjNeig{&map.at(MAP_IDX2)};

    PositionCheckableMock positionCheckableMock{};
    NeighborhoodFindableMock neighborhoodFindableMock{};
    QueueMock<ICommand> queueMock{};
    NeighborhoodFind neighborhoodFind{&neighborhoodFindableMock, &positionCheckableMock, &map};
    std::shared_ptr<ICommand> macroCommand;

    // Check adapter functions call
    EXPECT_CALL(positionCheckableMock, GetPosition()).WillOnce(Return(POS1));
    EXPECT_CALL(neighborhoodFindableMock, GetObjId()).WillOnce(Return(OBJ1_ID));
    EXPECT_CALL(neighborhoodFindableMock, GetPosition(OBJ2_ID)).WillOnce(Return(POS2));
    EXPECT_CALL(neighborhoodFindableMock, GetQueue()).WillOnce(Return(&queueMock));
    EXPECT_CALL(queueMock, Push(_)).WillOnce(SaveArg<0>(&macroCommand));

    EXPECT_NO_THROW(neighborhoodFind.Execute());

    // Check that new neighborhood have new object
    EXPECT_TRUE(newObjNeig->objects.find(OBJ1_ID) != newObjNeig->objects.end());
    EXPECT_TRUE(typeid(*macroCommand) == typeid(MacroCommand));

    EXPECT_CALL(positionCheckableMock, GetPosition).WillOnce(Return(POS1));

    // Check collision exception
    EXPECT_ANY_THROW(macroCommand->Execute());
}

TEST(PositionCheck, CollisionOneMap)
{
    constexpr std::uint64_t OBJ1_ID{12345};
    constexpr MoveData POS1{6, 5};
    constexpr auto MAP_IDX1{0};

    constexpr std::uint64_t OBJ2_ID{67891};
    constexpr MoveData POS2{POS1};
    constexpr auto MAP_IDX2{3};
    // Map (2x2) with neighborhood size = 4
    // 0  1
    // 2  3
    NeighborhoodMap map{};
    map.emplace(0, Neighborhood{0, 0, 4, {}});
    map.emplace(1, Neighborhood{4, 0, 4, {}});

    map.emplace(2, Neighborhood{0, 4, 4, {}});
    map.emplace(3, Neighborhood{4, 4, 4, {}});
    map.at(MAP_IDX1).objects.insert(OBJ1_ID);
    map.at(MAP_IDX2).objects.insert(OBJ2_ID);
    std::vector<NeighborhoodMap> maps{map};

    NeighborhoodMap *mapAddr{&map};
    Neighborhood *currentObjNeig{&map.at(MAP_IDX1)};
    Neighborhood *newObjNeig{&map.at(MAP_IDX2)};

    PositionCheckableMock positionCheckableMock{};
    NeighborhoodFindableMock neighborhoodFindableMock{};
    QueueMock<ICommand> queueMock{};
    CollisionCheck collisionCheck{&neighborhoodFindableMock, &positionCheckableMock, &maps};
    std::shared_ptr<ICommand> macroCommand;

    // Check adapter functions call
    EXPECT_CALL(positionCheckableMock, GetPosition()).WillOnce(Return(POS1));
    EXPECT_CALL(neighborhoodFindableMock, GetObjId()).WillOnce(Return(OBJ1_ID));
    EXPECT_CALL(neighborhoodFindableMock, GetPosition(OBJ2_ID)).WillOnce(Return(POS2));
    EXPECT_CALL(neighborhoodFindableMock, GetQueue()).WillOnce(Return(&queueMock));
    EXPECT_CALL(queueMock, Push(_)).WillOnce(SaveArg<0>(&macroCommand));

    EXPECT_NO_THROW(collisionCheck.Execute());

    EXPECT_TRUE(typeid(*macroCommand) == typeid(MacroCommand));

    EXPECT_CALL(positionCheckableMock, GetPosition).WillOnce(Return(POS1));

    // Check collision exception
    EXPECT_ANY_THROW(macroCommand->Execute());
}

TEST(PositionCheck, CollisionTwoMap)
{
    constexpr std::uint64_t OBJ1_ID{12345};
    constexpr MoveData POS1{6, 5};
    constexpr auto MAP_IDX1{0};

    constexpr std::uint64_t OBJ2_ID{67891};
    constexpr MoveData POS2{POS1};
    constexpr auto MAP_IDX2{3};
    // Map (2x2) with neighborhood size = 4
    // 0  1
    // 2  3
    NeighborhoodMap map{};
    map.emplace(0, Neighborhood{0, 0, 4, {}});
    map.emplace(1, Neighborhood{4, 0, 4, {}});

    map.emplace(2, Neighborhood{0, 4, 4, {}});
    map.emplace(3, Neighborhood{4, 4, 4, {}});
    map.at(MAP_IDX1).objects.insert(OBJ1_ID);
    map.at(MAP_IDX2).objects.insert(OBJ2_ID);

    // Add the same map twice for testing
    std::vector<NeighborhoodMap> maps{map, map};

    NeighborhoodMap *mapAddr{&map};
    Neighborhood *currentObjNeig{&map.at(MAP_IDX1)};
    Neighborhood *newObjNeig{&map.at(MAP_IDX2)};

    PositionCheckableMock positionCheckableMock{};
    NeighborhoodFindableMock neighborhoodFindableMock{};
    QueueMock<ICommand> queueMock{};
    CollisionCheck collisionCheck{&neighborhoodFindableMock, &positionCheckableMock, &maps};

    std::shared_ptr<ICommand> macroCommand1;
    std::shared_ptr<ICommand> macroCommand2;

    // Check adapter functions call
    EXPECT_CALL(positionCheckableMock, GetPosition()).WillRepeatedly(Return(POS1));
    EXPECT_CALL(neighborhoodFindableMock, GetObjId()).WillRepeatedly(Return(OBJ1_ID));
    EXPECT_CALL(neighborhoodFindableMock, GetPosition(OBJ2_ID)).WillRepeatedly(Return(POS2));
    EXPECT_CALL(neighborhoodFindableMock, GetQueue()).WillRepeatedly(Return(&queueMock));
    EXPECT_CALL(queueMock, Push(_)).WillOnce(SaveArg<0>(&macroCommand1)).WillOnce(SaveArg<0>(&macroCommand2));

    EXPECT_NO_THROW(collisionCheck.Execute());

    // First macro with position check commands
    EXPECT_TRUE(typeid(*macroCommand1) == typeid(MacroCommand));
    EXPECT_CALL(positionCheckableMock, GetPosition).WillOnce(Return(POS1));
    EXPECT_ANY_THROW(macroCommand1->Execute());
    
    // Second macro with position check commands
    EXPECT_TRUE(typeid(*macroCommand1) == typeid(MacroCommand));
    EXPECT_CALL(positionCheckableMock, GetPosition).WillOnce(Return(POS1));
    EXPECT_ANY_THROW(macroCommand1->Execute());
}
