#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <iostream>
#include <chrono>

#include "cmd_handler.hpp"
#include "mocks.hpp"

using namespace ::testing;

TEST(CmdHandler, SoftStop)
{
    auto cmdMock = std::make_unique<CommandMock>();
    auto cmdMock1 = std::make_unique<CommandMock>();
    auto cmdMock2 = std::make_unique<CommandMock>();

    ThreadStrategy threadStrategy{};

    EXPECT_CALL(*cmdMock, Execute).Times(1);
    EXPECT_CALL(*cmdMock1, Execute).Times(1);
    EXPECT_CALL(*cmdMock2, Execute).Times(1);

    threadStrategy.GetQueue()->Push(std::move(cmdMock));
    threadStrategy.GetQueue()->Push(std::move(cmdMock1));
    threadStrategy.GetQueue()->Push(std::make_unique<ThreadSoftStop>());
    threadStrategy.GetQueue()->Push(std::move(cmdMock2));

    std::this_thread::sleep_for(std::chrono::milliseconds(100));
}

TEST(CmdHandler, HardStop)
{
    auto cmdMock = std::make_unique<CommandMock>();
    auto cmdMock1 = std::make_unique<CommandMock>();
    auto cmdMock2 = std::make_unique<CommandMock>();

    ThreadStrategy threadStrategy{};

    EXPECT_CALL(*cmdMock, Execute).Times(1);
    EXPECT_CALL(*cmdMock1, Execute).Times(1);
    EXPECT_CALL(*cmdMock2, Execute).Times(0);

    threadStrategy.GetQueue()->Push(std::move(cmdMock));
    threadStrategy.GetQueue()->Push(std::move(cmdMock1));
    threadStrategy.GetQueue()->Push(std::make_unique<ThreadHardStop>());
    threadStrategy.GetQueue()->Push(std::move(cmdMock2));

    std::this_thread::sleep_for(std::chrono::milliseconds(100));
}

TEST(CmdHandler, MoveTo)
{
    auto cmdMock = std::make_unique<CommandMock>();
    auto cmdMock1 = std::make_unique<CommandMock>();
    auto cmdMock2 = std::make_unique<CommandMock>();
    auto queueForMove = std::make_shared<QueueMock<ICommand>>();

    ThreadStrategy threadStrategy{};

    EXPECT_CALL(*cmdMock, Execute).Times(1);
    EXPECT_CALL(*cmdMock1, Execute).Times(1);
    EXPECT_CALL(*cmdMock2, Execute).Times(0);
    EXPECT_CALL(*queueForMove, Push(_)).Times(1);

    threadStrategy.GetQueue()->Push(std::move(cmdMock));
    threadStrategy.GetQueue()->Push(std::move(cmdMock1));
    threadStrategy.GetQueue()->Push(std::make_shared<ThreadMoveTo>(queueForMove));
    threadStrategy.GetQueue()->Push(std::move(cmdMock2));
    threadStrategy.GetQueue()->Push(std::make_shared<ThreadHardStop>());

    std::this_thread::sleep_for(std::chrono::milliseconds(100));
}

TEST(CmdHandler, Run)
{
    auto cmdMock = std::make_unique<CommandMock>();
    auto cmdMock1 = std::make_unique<CommandMock>();
    auto cmdMock2 = std::make_unique<CommandMock>();
    auto cmdMock3 = std::make_unique<CommandMock>();

    auto queueForMove = std::make_shared<QueueMock<ICommand>>();

    ThreadStrategy threadStrategy{};

    EXPECT_CALL(*cmdMock, Execute).Times(1);
    EXPECT_CALL(*cmdMock1, Execute).Times(1);
    EXPECT_CALL(*cmdMock2, Execute).Times(0);
    EXPECT_CALL(*cmdMock3, Execute).Times(1);
    EXPECT_CALL(*queueForMove, Push(_)).Times(1);

    threadStrategy.GetQueue()->Push(std::move(cmdMock));
    threadStrategy.GetQueue()->Push(std::move(cmdMock1));
    threadStrategy.GetQueue()->Push(std::make_shared<ThreadMoveTo>(queueForMove));
    threadStrategy.GetQueue()->Push(std::move(cmdMock2));
    threadStrategy.GetQueue()->Push(std::make_shared<ThreadRun>());
    threadStrategy.GetQueue()->Push(std::move(cmdMock3));
    threadStrategy.GetQueue()->Push(std::make_shared<ThreadHardStop>());

    std::this_thread::sleep_for(std::chrono::milliseconds(100));
}
