#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <stdexcept>

#include "mocks.hpp"
#include "rotate.hpp"

using namespace ::testing;

ACTION_P(ThrowException, str)
{
    throw std::runtime_error{str};
}

TEST(Rotate, Normal)
{
    constexpr int START_DIRECTION{3};
    constexpr int ANGULAR_VELOCITY{-46};
    constexpr int DIRECTIONS_NUMBER{36};
    constexpr int STOP_DIRECTION{-7};

    RotatableMock mock{};
    Rotate rotate{&mock};

    EXPECT_CALL(mock, GetDirection).WillOnce(Return(START_DIRECTION));
    EXPECT_CALL(mock, GetAngularVelocity).WillOnce(Return(ANGULAR_VELOCITY));
    EXPECT_CALL(mock, GetDirectionsNumber).WillOnce(Return(DIRECTIONS_NUMBER));
    EXPECT_CALL(mock, SetDirection(STOP_DIRECTION));

    EXPECT_NO_THROW(rotate.Execute());
}

TEST(Rotate, GetDirectionError)
{
    RotatableMock mock{};
    Rotate rotate{&mock};

    EXPECT_CALL(mock, GetDirection).WillOnce(ThrowException("Get direction error"));

    EXPECT_ANY_THROW(rotate.Execute());
}

TEST(Rotate, GetVelocityError)
{
    RotatableMock mock{};
    Rotate rotate{&mock};

    EXPECT_CALL(mock, GetAngularVelocity).WillOnce(ThrowException("Get angular velocity error"));

    EXPECT_ANY_THROW(rotate.Execute());
}

TEST(Rotate, GetDirectionsNumberError)
{
    RotatableMock mock{};
    Rotate rotate{&mock};

    EXPECT_CALL(mock, GetDirectionsNumber).WillOnce(ThrowException("Get directions number error"));

    EXPECT_ANY_THROW(rotate.Execute());
}

TEST(Rotate, SetDirectionError)
{
    constexpr int DIRECTIONS_NUMBER{36};

    RotatableMock mock{};
    Rotate rotate{&mock};

    EXPECT_CALL(mock, GetDirectionsNumber).WillOnce(Return(DIRECTIONS_NUMBER)); // to avoid division by zero
    EXPECT_CALL(mock, SetDirection).WillOnce(ThrowException("Set direction error"));

    EXPECT_ANY_THROW(rotate.Execute());
}
