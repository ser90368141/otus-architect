#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <stdexcept>

#include "fuel_burn.hpp"
#include "mocks.hpp"

using namespace ::testing;

TEST(FuelBurn, Normal)
{
    constexpr int LEVEL_START{12};
    constexpr int CONSUMPTION{5};
    constexpr int LEVEL_STOP{LEVEL_START - CONSUMPTION};

    FuelBurnableMock mock{};
    FuelBurn fuelBurn{&mock};

    EXPECT_CALL(mock, GetConsumption).WillOnce(Return(CONSUMPTION));
    EXPECT_CALL(mock, GetLevel).WillOnce(Return(LEVEL_START));
    EXPECT_CALL(mock, SetLevel(LEVEL_STOP));

    EXPECT_NO_THROW(fuelBurn.Execute());
}

TEST(FuelBurn, Zero)
{
    constexpr int LEVEL_START{12};
    constexpr int CONSUMPTION{50};
    constexpr int LEVEL_STOP{0};

    FuelBurnableMock mock{};
    FuelBurn fuelBurn{&mock};

    EXPECT_CALL(mock, GetConsumption).WillOnce(Return(CONSUMPTION));
    EXPECT_CALL(mock, GetLevel).WillOnce(Return(LEVEL_START));
    EXPECT_CALL(mock, SetLevel(LEVEL_STOP));

    EXPECT_NO_THROW(fuelBurn.Execute());
}
