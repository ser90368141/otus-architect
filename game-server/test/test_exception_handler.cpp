#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <stdexcept>
#include <typeinfo>
#include <typeindex>
#include <iostream>
#include <exception>

#include "exception_handler.hpp"
#include "mocks.hpp"
#include "repeater.hpp"

using namespace ::testing;
namespace
{
    using CommandQueue = std::queue<std::unique_ptr<ICommand>>;
}

ACTION(ThrowCommandException)
{
    throw CommandException{};
}
ACTION(ThrowRuntimeException)
{
    throw std::runtime_error{"RuntimeError"};
}

TEST(ExceptionHandler, Test)
{
    ExcHandler excHandler{};
    TestMock testMock{};
    TestMock testMock1{};

    excHandler.AddHandler(std::type_index(typeid(CommandMock)), std::type_index(typeid(CommandException)), [&testMock](std::unique_ptr<ICommand>, std::exception &)
                          { testMock.Test(); });
    excHandler.AddHandler(std::type_index(typeid(CommandMock)), std::type_index(typeid(std::runtime_error)), [&testMock1](std::unique_ptr<ICommand>, std::exception &)
                          { testMock1.Test(); });

    auto cmdMock = std::make_unique<CommandMock>();
    EXPECT_CALL(*cmdMock, Execute).WillOnce(ThrowCommandException());
    EXPECT_CALL(testMock, Test).Times(1);
    try
    {
        cmdMock->Execute();
    }
    catch (std::exception &e)
    {
        excHandler.Handle(std::move(cmdMock), e);
    }

    auto cmdMock1 = std::make_unique<CommandMock>();
    EXPECT_CALL(*cmdMock1, Execute).WillOnce(ThrowRuntimeException());
    EXPECT_CALL(testMock1, Test).Times(1);
    try
    {
        cmdMock1->Execute();
    }
    catch (std::exception &e)
    {
        excHandler.Handle(std::move(cmdMock1), e);
    }
}

TEST(ExceptionHandler, TestQueue)
{
    CommandQueue queue{};
    TestMock testMock{};
    TestMock testMock1{};

    ExcHandler excHandler{};
    excHandler.AddHandler(std::type_index(typeid(CommandMock)), std::type_index(typeid(CommandException)), [&testMock](std::unique_ptr<ICommand>, std::exception &)
                          { testMock.Test(); });
    excHandler.AddHandler(std::type_index(typeid(CommandMock)), std::type_index(typeid(std::runtime_error)), [&testMock1](std::unique_ptr<ICommand>, std::exception &)
                          { testMock1.Test(); });

    auto cmdMock = std::make_unique<CommandMock>();
    auto cmdMock1 = std::make_unique<CommandMock>();
    EXPECT_CALL(*cmdMock, Execute).WillOnce(ThrowCommandException());
    EXPECT_CALL(*cmdMock1, Execute).WillOnce(ThrowRuntimeException());
    queue.push(std::move(cmdMock));
    queue.push(std::move(cmdMock1));
    EXPECT_CALL(testMock, Test).Times(1);
    EXPECT_CALL(testMock1, Test).Times(1);

    while (!queue.empty())
    {
        auto cmd = std::move(queue.front());
        queue.pop();
        try
        {

            cmd->Execute();
        }
        catch (std::exception &e)
        {
            excHandler.Handle(std::move(cmd), e);
        }
    }
}

TEST(ExceptionHandler, WriteLog)
{
    CommandQueue queue{};
    LogableMock logableMock{};

    ExcHandler excHandler{};
    excHandler.AddHandler(std::type_index(typeid(CommandMock)), std::type_index(typeid(CommandException)), [&logableMock, &queue](std::unique_ptr<ICommand>, std::exception &)
                          {
        auto log = std::make_unique<Log>(&logableMock);
        queue.push(std::move(log)); });

    auto cmdMock = std::make_unique<CommandMock>();
    EXPECT_CALL(*cmdMock, Execute).WillOnce(ThrowCommandException());
    queue.push(std::move(cmdMock));

    EXPECT_CALL(logableMock, Write).Times(1);

    while (!queue.empty())
    {
        auto cmd = std::move(queue.front());
        queue.pop();
        try
        {
            cmd->Execute();
        }
        catch (std::exception &e)
        {
            excHandler.Handle(std::move(cmd), e);
        }
    }
}

TEST(ExceptionHandler, RepeatCommand)
{
    CommandQueue queue{};

    ExcHandler excHandler{};
    excHandler.AddHandler(std::type_index(typeid(CommandMock)), std::type_index(typeid(CommandException)), [&queue](std::unique_ptr<ICommand> cmd, std::exception &)
                          {
        auto repeater = std::make_unique<Repeater>(std::move(cmd));
        queue.push(std::move(repeater)); });

    auto cmdMock = std::make_unique<CommandMock>();
    EXPECT_CALL(*cmdMock, Execute).WillOnce(ThrowCommandException()).WillOnce(DoDefault());
    queue.push(std::move(cmdMock));

    while (!queue.empty())
    {
        auto cmd = std::move(queue.front());
        queue.pop();
        try
        {
            cmd->Execute();
        }
        catch (std::exception &e)
        {
            excHandler.Handle(std::move(cmd), e);
        }
    }
}

TEST(ExceptionHandler, RepeatAndWriteLog)
{
    CommandQueue queue{};
    LogableMock logableMock{};

    ExcHandler excHandler{};
    excHandler.AddHandler(std::type_index(typeid(Repeater)), std::type_index(typeid(CommandException)), [&logableMock, &queue](std::unique_ptr<ICommand>, std::exception &)
                          {
        auto log = std::make_unique<Log>(&logableMock);
        queue.push(std::move(log)); });
    excHandler.AddHandler(std::type_index(typeid(CommandMock)), std::type_index(typeid(CommandException)), [&queue](std::unique_ptr<ICommand> cmd, std::exception &)
                          {
        auto repeater = std::make_unique<Repeater>(std::move(cmd));
        queue.push(std::move(repeater)); });

    auto cmdMock = std::make_unique<CommandMock>();
    EXPECT_CALL(*cmdMock, Execute).Times(2).WillRepeatedly(ThrowCommandException());
    queue.push(std::move(cmdMock));

    EXPECT_CALL(logableMock, Write).Times(1);

    while (!queue.empty())
    {
        auto cmd = std::move(queue.front());
        queue.pop();
        try
        {
            cmd->Execute();
        }
        catch (std::exception &e)
        {
            excHandler.Handle(std::move(cmd), e);
        }
    }
}

TEST(ExceptionHandler, DoubleRepeatAndWriteLog)
{
    CommandQueue queue{};
    LogableMock logableMock{};

    ExcHandler excHandler{};
    excHandler.AddHandler(std::type_index(typeid(DoubleRepeater)), std::type_index(typeid(CommandException)), [&logableMock, &queue](std::unique_ptr<ICommand>, std::exception &)
                          {
        auto log = std::make_unique<Log>(&logableMock);
        queue.push(std::move(log)); });
    excHandler.AddHandler(std::type_index(typeid(Repeater)), std::type_index(typeid(CommandException)), [&logableMock, &queue](std::unique_ptr<ICommand> cmd, std::exception &)
                          {
        auto doubleRepeater = std::make_unique<DoubleRepeater>(std::move(cmd));
        queue.push(std::move(doubleRepeater)); });
    excHandler.AddHandler(std::type_index(typeid(CommandMock)), std::type_index(typeid(CommandException)), [&queue](std::unique_ptr<ICommand> cmd, std::exception &)
                          {
        auto repeater = std::make_unique<Repeater>(std::move(cmd));
        queue.push(std::move(repeater)); });

    auto cmdMock = std::make_unique<CommandMock>();
    EXPECT_CALL(*cmdMock, Execute).Times(3).WillRepeatedly(ThrowCommandException());
    queue.push(std::move(cmdMock));

    EXPECT_CALL(logableMock, Write).Times(1);

    while (!queue.empty())
    {
        auto cmd = std::move(queue.front());
        queue.pop();
        try
        {
            cmd->Execute();
        }
        catch (std::exception &e)
        {
            excHandler.Handle(std::move(cmd), e);
        }
    }
}