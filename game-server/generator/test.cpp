#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <string>
#include <string_view>
#include <fstream>

#include "generator.hpp"

namespace
{
    constexpr auto TMP_INPUT_FILE{"tmp_input.hpp"};
}

using namespace ::testing;

TEST(GeneratorTests, ParseInvalidNamePrefix)
{
    constexpr auto TEST_SOURCE{R"(class Testable {
    };)"};

    {
        std::ofstream file(TMP_INPUT_FILE);
        file << TEST_SOURCE;
        file.close();
    }

    EXPECT_ANY_THROW(Generator::ParseFile(TMP_INPUT_FILE));
}

TEST(GeneratorTests, ParseInvalidNameSuffix)
{
    constexpr auto TEST_SOURCE{R"(class ITest {
    };)"};

    {
        std::ofstream file(TMP_INPUT_FILE);
        file << TEST_SOURCE;
        file.close();
    }

    EXPECT_ANY_THROW(Generator::ParseFile(TMP_INPUT_FILE));
}

TEST(GeneratorTests, ParseNotPureVirtual)
{
    constexpr auto TEST_SOURCE{R"(class ITestable {
        int GetInt() { return 0; }
    };)"};

    {
        std::ofstream file(TMP_INPUT_FILE);
        file << TEST_SOURCE;
        file.close();
    }

    EXPECT_ANY_THROW(Generator::ParseFile(TMP_INPUT_FILE));
}

TEST(GeneratorTests, ParseNormal)
{
    constexpr auto TEST_SOURCE{R"(class ITestable {
        virtual int GetInt() = 0;
    };)"};

    {
        std::ofstream file(TMP_INPUT_FILE);
        file << TEST_SOURCE;
        file.close();
    }

    EXPECT_NO_THROW(Generator::ParseFile(TMP_INPUT_FILE));
}

TEST(GeneratorTests, GenerateWrongFieldName)
{
    const auto jsonData = inja::json{{"className", "TestClass"}, {"interfaceName", "TestInterface"}};
    constexpr auto TEMPLATE{R"(class {{ classNamee }} : public {{ interfaceName }}
{
public:
    {{ className }}(): interfaceName{} {}
    ~{{ className }}() = default;
};)"};

    EXPECT_ANY_THROW(Generator::Generate(TEMPLATE, jsonData));
}

TEST(GeneratorTests, GenerateSuccess)
{
    const auto jsonData = inja::json{{"className", "TestClass"}, {"interfaceName", "TestInterface"}};
    constexpr auto TEMPLATE{R"(class {{ className }} : public {{ interfaceName }}
{
public:
    {{ className }}(): interfaceName{} {}
    ~{{ className }}() = default;
};)"};
    constexpr auto RESULT{R"(class TestClass : public TestInterface
{
public:
    TestClass(): interfaceName{} {}
    ~TestClass() = default;
};)"};


    EXPECT_NO_THROW(Generator::Generate(TEMPLATE, jsonData));
    EXPECT_TRUE(Generator::Generate(TEMPLATE, jsonData) == RESULT);
}
