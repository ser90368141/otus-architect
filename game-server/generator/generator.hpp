#include <memory>
#include <cppast/cpp_namespace.hpp>
#include <cppast/cpp_member_function.hpp>
#include <cppast/cpp_class.hpp>
#include <cppast/libclang_parser.hpp>
#include <iostream>
#include <string>
#include <string_view>
#include <inja/inja.hpp>
#include <stdexcept>
#include <exception>

class Generator
{
private:
    static inline std::unique_ptr<cppast::cpp_file> ParseFileAst(std::string_view filePath)
    {
        cppast::libclang_compile_config config{};
        cppast::stderr_diagnostic_logger logger{};
        cppast::cpp_entity_index idx{};
        cppast::libclang_parser parser(type_safe::ref(logger));

        auto file = parser.parse(idx, filePath.data(), config);
        if (parser.error() || !file)
        {
            throw std::runtime_error{"AST parsing error"};
            return nullptr;
        }
        return file;
    }

    static inline void CheckIsInterface(const cppast::cpp_class &classEntry)
    {
        // Check name
        {
            auto className{classEntry.name()};
            if (className.find("ble") == std::string::npos)
            {
                throw std::runtime_error{"Interface not end with \"ble\""};
            }

            if (className[0] != 'I')
            {
                throw std::runtime_error{"Interface not start with \"I\""};
            }
        }

        // Check destructor
        for (auto &entry : classEntry)
        {
            if (entry.kind() != cppast::cpp_entity_kind::destructor_t)
            {
                continue;
            }

            auto &dtorEntry = static_cast<const cppast::cpp_destructor &>(entry);
            if (!dtorEntry.is_virtual() || dtorEntry.body_kind() != cppast::cpp_function_body_kind::cpp_function_defaulted)
            {
                throw std::runtime_error{"Interface does not have default virtual destructor"};
            }
        }

        // Check member functions
        for (auto &entry : classEntry)
        {
            if (entry.kind() != cppast::cpp_entity_kind::member_function_t)
            {
                continue;
            }
            auto &funcEntry = static_cast<const cppast::cpp_member_function &>(entry);
            if (!funcEntry.is_virtual() || !funcEntry.virtual_info().value().is_set(cppast::cpp_virtual_flags::pure))
            {
                throw std::runtime_error{"Interface have some not pure virtual member function"};
            }
        }
    }

    static inline inja::json PrepareJson(const cppast::cpp_file &file)
    {
        inja::json j{};

        // Parse file name
        auto fileName{file.name()};
        fileName = fileName.substr(fileName.rfind("/") + 1);
        std::cout << "File name: " << fileName << std::endl;
        if (fileName.empty())
        {
            throw std::runtime_error{"File name is empty"};
        }

        // Parse file
        for (auto &fileEntry : file)
        {
            if (fileEntry.kind() != cppast::cpp_entity_kind::class_t)
            {
                continue;
            }

            auto &classEntry = static_cast<const cppast::cpp_class &>(fileEntry);
            // Check class is needed interface
            try
            {
                CheckIsInterface(classEntry);
            }
            catch (const std::exception &e)
            {
                continue;
            }

            std::cout << "Class: " << classEntry.name() << std::endl;
            j["interfaceName"] = classEntry.name();
            j["className"] = classEntry.name().substr(1);

            inja::json::array_t getFuncJson{};
            inja::json::array_t setFuncJson{};
            for (auto &entry : classEntry)
            {
                if (entry.kind() != cppast::cpp_entity_kind::member_function_t)
                {
                    continue;
                }
                auto &funcEntry = static_cast<const cppast::cpp_member_function &>(entry);

                // Check function type
                if (funcEntry.name().substr(0, 3).find("Get") != std::string::npos)
                {
                    // Getter
                    std::cout << "Getter function:" << funcEntry.name() << std::endl;
                    inja::json funcJson{};
                    funcJson["name"] = funcEntry.name();
                    funcJson["property"] = funcEntry.name().substr(3, funcEntry.name().size());
                    funcJson["type"] = cppast::to_string(funcEntry.return_type());
                    getFuncJson.push_back(funcJson);
                }
                else if (funcEntry.name().substr(0, 3).find("Set") != std::string::npos)
                {
                    // Setter
                    std::cout << "Setter function:" << funcEntry.name() << std::endl;
                    inja::json funcJson{};
                    funcJson["name"] = funcEntry.name();
                    funcJson["property"] = funcEntry.name().substr(3, funcEntry.name().size());

                    inja::json::array_t paramArrayJson{};
                    for (auto &param : funcEntry.parameters())
                    {
                        inja::json paramJson{};
                        paramJson["type"] = cppast::to_string(param.type());
                        paramJson["name"] = param.name();
                        paramArrayJson.push_back(paramJson);
                    }
                    funcJson["params"] = paramArrayJson;

                    setFuncJson.push_back(funcJson);
                }
            }
            if (!getFuncJson.empty())
            {
                j["getFuncArrayExist"] = true;
                j["getFuncArray"] = getFuncJson;
            }
            else
            {
                j["getFuncArrayExist"] = false;
            }
            if (!setFuncJson.empty())
            {
                j["setFuncArrayExist"] = true;
                j["setFuncArray"] = setFuncJson;
            }
            else
            {
                j["setFuncArrayExist"] = false;
            }
        }

        if (j.empty())
        {
            throw std::runtime_error{"File does not have any interface for generating"};
        }

        j["fileName"] = fileName;
        return j;
    }

public:
    static inline inja::json ParseFile(std::string_view filePath)
    {
        auto parsedFile{ParseFileAst(filePath)};
        return PrepareJson(*parsedFile);
    }

    static inline std::string Generate(std::string_view templ, const inja::json &jsonData)
    {
        return inja::render(templ, jsonData);
    }
};