#include <iostream>
#include <string>
#include <string_view>
#include <fstream>

#include "generator.hpp"

int main(int argc, char **argv)
{
    if (argc < 3)
    {
        std::cout << "Usage: gen <template file> <output file> <input file 1> <input file 2> ... " << std::endl;
        return -1;
    }
    std::string_view templateFilePath{argv[1]};
    std::string_view outputFilePath{argv[2]};

    inja::json::array_t parsedFilesData{};
    for (int i = 3; i < argc; ++i)
    {

        try
        {
            const auto parsedData = Generator::ParseFile(argv[i]);
            parsedFilesData.push_back(parsedData);
        }
        catch (const std::exception &e)
        {
            std::cerr << "File parse error: " << e.what() << std::endl;
            continue;
        }
    }
    inja::json jsonData{{"classArray", parsedFilesData}};

    std::ifstream templateFile;
    templateFile.open(templateFilePath.data());
    if (templateFile.fail())
    {
        std::cerr << "File template open error" << std::endl;
        return -1;
    }
    std::string templateString{(std::istreambuf_iterator<char>(templateFile)), std::istreambuf_iterator<char>()};

    const auto outputFileString{Generator::Generate(templateString, jsonData)};

    std::ofstream outputFile(outputFilePath.data());
    if (!outputFile)
    {
        std::cerr << "Output file open error!" << std::endl;
        return -1;
    }
    outputFile << outputFileString;
    outputFile.close();

    std::cout << "Code generated" << std::endl;
    return 0;
}
