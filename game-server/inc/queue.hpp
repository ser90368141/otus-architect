#pragma once

#include "command.hpp"
#include "ioc.hpp"

#include <queue>
#include <mutex>
#include <condition_variable>
#include <memory>
#include <thread>
#include <iostream>
#include <functional>
#include <chrono>
#include <cstdint>
#include <cstddef>
#include <typeinfo>
#include <typeindex>

using namespace std::literals::string_view_literals;

template <typename T>
class IQueue
{
public:
    virtual ~IQueue() = default;

    virtual void Push(std::shared_ptr<T> cmd) = 0;
    virtual void PushUnsave(std::shared_ptr<T> cmd) = 0;
    virtual std::shared_ptr<T> Pop() = 0;
    virtual std::size_t GetSize() = 0;
};

template <typename T>
class SafeQueue : public IQueue<T>
{
    std::queue<std::shared_ptr<T>> queue;
    mutable std::mutex mutex;
    std::condition_variable conVar;

public:
    SafeQueue() : queue{}, mutex{}, conVar{} {}
    ~SafeQueue() = default;

    void Push(std::shared_ptr<T> cmd) override
    {
        std::lock_guard<std::mutex> lock(mutex);
        queue.push(std::move(cmd));
        conVar.notify_one();
    }

    void PushUnsave(std::shared_ptr<T> cmd) override
    {
        queue.push(std::move(cmd));
        conVar.notify_one();
    }

    std::shared_ptr<T> Pop() override
    {
        std::unique_lock<std::mutex> lock(mutex);
        while (queue.empty())
        {
            // release lock as long as the wait and reaquire it afterwards.
            conVar.wait(lock);
        }
        auto cmd = std::move(queue.front());
        queue.pop();
        return cmd;
    }

    std::size_t GetSize() override
    {
        std::size_t size{0};
        {
            std::lock_guard<std::mutex> lock(mutex);
            size = queue.size();
        }
        return size;
    }
};

template <typename T>
class SimpleQueue : public IQueue<T>
{
    std::queue<std::shared_ptr<T>> queue{};

public:
    SimpleQueue() : queue{} {}
    ~SimpleQueue() = default;

    void Push(std::shared_ptr<T> cmd) override
    {
        queue.push(std::move(cmd));
    }

    void PushUnsave(std::shared_ptr<T> cmd) override
    {
        Push(std::move(cmd));
    }

    std::shared_ptr<T> Pop() override
    {
        if (queue.empty())
            return nullptr;

        auto cmd = std::move(queue.front());
        queue.pop();
        return cmd;
    }

    std::size_t GetSize() override
    {
        return queue.size();
    }
};
