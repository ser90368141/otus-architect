#pragma once

#include "command.hpp"
#include <exception>

class IFuelCheckable
{
public:
    virtual ~IFuelCheckable() = default;

    virtual int GetConsumption() = 0;
    virtual int GetLevel() = 0;
};

class FuelCheck : public ICommand
{
    IFuelCheckable *item;

public:
    FuelCheck(IFuelCheckable *item) : item{item}
    {
    }
    void Execute() override
    {
        if (item->GetLevel() < item->GetConsumption())
        {
            throw CommandException{};
        }
    }
};
