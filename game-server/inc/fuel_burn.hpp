#pragma once

#include "command.hpp"

class IFuelBurnable
{
public:
    virtual ~IFuelBurnable() = default;

    virtual int GetConsumption() = 0;
    virtual int GetLevel() = 0;
    virtual void SetLevel(int level) = 0;
};

class FuelBurn : public ICommand
{
    IFuelBurnable *item;

public:
    FuelBurn(IFuelBurnable *item) : item{item}
    {
    }
    void Execute() override
    {
        const auto newLevel = item->GetLevel() - item->GetConsumption();
        item->SetLevel(newLevel > 0 ? newLevel : 0);
    }
};
