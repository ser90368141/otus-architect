#pragma once

#include <memory>

#include "command.hpp"

class BaseRepeater : public ICommand
{
    std::unique_ptr<ICommand> cmd;

public:
    BaseRepeater(std::unique_ptr<ICommand> cmd) : cmd{std::move(cmd)}
    {
    }
    void Execute() override
    {
        cmd->Execute();
    }
};

class Repeater : public BaseRepeater
{
public:
    Repeater(std::unique_ptr<ICommand> cmd) : BaseRepeater{std::move(cmd)}
    {
    }
};

class DoubleRepeater : public BaseRepeater
{
public:
    DoubleRepeater(std::unique_ptr<ICommand> cmd) : BaseRepeater{std::move(cmd)}
    {
    }
};
