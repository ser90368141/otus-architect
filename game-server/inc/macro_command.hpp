#pragma once

#include "command.hpp"
#include <vector>
#include <utility>
#include <exception>
#include <memory>
#include <utility>

class MacroCommand : public ICommand
{
public:
    using CmdArray = std::vector<std::shared_ptr<ICommand>>;

    MacroCommand(CmdArray cmdArr) : cmdArray{std::move(cmdArr)}
    {
    }
    void Execute() override
    {
        try
        {
            for (auto &cmd : cmdArray)
                cmd->Execute();
        }
        catch (const std::exception &e)
        {
            throw CommandException{};
        }
    }

private:
    CmdArray cmdArray{};
};

class MoveWithBurn : public ICommand
{
private:
    MacroCommand macro;

public:
    MoveWithBurn(std::shared_ptr<ICommand> fuelCheckCmd, std::shared_ptr<ICommand> moveCmd, std::shared_ptr<ICommand> fuelBurnCmd)
        : macro{MacroCommand::CmdArray{std::move(fuelCheckCmd), std::move(moveCmd), std::move(fuelBurnCmd)}}
    {
    }
    void Execute() override
    {
        macro.Execute();
    }
};

class MoveWithBurnVelocityChange : public ICommand
{
private:
    MacroCommand macro;

public:
    MoveWithBurnVelocityChange(std::shared_ptr<ICommand> fuelCheckCmd, std::shared_ptr<ICommand> moveCmd, std::shared_ptr<ICommand> fuelBurnCmd, std::shared_ptr<ICommand> velocityChangeCmd)
        : macro{MacroCommand::CmdArray{std::move(fuelCheckCmd), std::move(moveCmd), std::move(fuelBurnCmd), std::move(velocityChangeCmd)}}
    {
    }
    void Execute() override
    {
        macro.Execute();
    }
};