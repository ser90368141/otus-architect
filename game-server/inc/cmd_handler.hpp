#pragma once

#include "command.hpp"
#include "ioc.hpp"
#include "queue.hpp"

#include <memory>
#include <thread>
#include <iostream>
#include <functional>
#include <chrono>
#include <cstdint>
#include <cstddef>
#include <typeinfo>
#include <typeindex>

using namespace std::literals::string_view_literals;

class ThreadQueue : public SafeQueue<ICommand>
{
public:
    ThreadQueue() = default;
    ~ThreadQueue() = default;
};

class ThreadHardStop : public ICommand
{
public:
    ThreadHardStop() {}
    void Execute() override;
};

class ThreadRun : public ICommand
{
public:
    ThreadRun() {}
    void Execute() override;
};

class ThreadSoftStop : public ICommand
{
public:
    ThreadSoftStop() {}
    void Execute() override;
};

class ThreadMoveTo : public ICommand
{
    std::shared_ptr<IQueue<ICommand>> m_queue;

public:
    ThreadMoveTo(std::shared_ptr<IQueue<ICommand>> queue) : m_queue{queue}
    {
    }
    void Execute() override;
};

class ThreadStrategy
{
    class State
    {
    protected:
        ThreadStrategy *m_context;

    public:
        virtual ~State(){};
        virtual bool Handle() = 0;
    };

    class StateNormal : public State
    {
    public:
        StateNormal(ThreadStrategy *context)
        {
            m_context = context;
        }
        bool Handle() override
        {
            auto cmd = std::move(m_context->queue->Pop());
            try
            {
                cmd->Execute();
            }
            catch (std::exception &e)
            {
                IoC::Resolve<std::shared_ptr<ICommand>>("Exception.Handler", &typeid(cmd), e)->Execute();
            }
            return true;
        }
    };

    class StateSoftStop : public State
    {
    public:
        StateSoftStop(ThreadStrategy *context)
        {
            m_context = context;
        }
        bool Handle() override
        {
            if (m_context->softStopQueueSize)
            {
                --m_context->softStopQueueSize;
                auto cmd = std::move(m_context->queue->Pop());
                try
                {
                    cmd->Execute();
                }
                catch (std::exception &e)
                {
                    IoC::Resolve<std::shared_ptr<ICommand>>("Exception.Handler", &typeid(cmd), e)->Execute();
                }
                return true;
            }
            return false;
        }
    };

    class StateHardStop : public State
    {
    public:
        StateHardStop(ThreadStrategy *context)
        {
            m_context = context;
        }
        bool Handle() override
        {
            return false;
        }
    };

    class StateMoveTo : public State
    {
    public:
        StateMoveTo(ThreadStrategy *context)
        {
            m_context = context;
        }
        bool Handle() override
        {
            auto cmd = std::move(m_context->queue->Pop());
            std::cout << typeid(*cmd).name() << " : " << typeid(ThreadHardStop).name() << std::endl;
            if (typeid(*cmd) == typeid(ThreadHardStop) || typeid(*cmd) == typeid(ThreadRun))
            {
                cmd->Execute();
                return true;
            }
            if (m_context->queueForMove)
                m_context->queueForMove->Push(std::move(cmd));
            return true;
        }
    };

    std::unique_ptr<IQueue<ICommand>> queue{nullptr};
    std::shared_ptr<IQueue<ICommand>> queueForMove{nullptr};
    State *m_state{};
    std::size_t softStopQueueSize{};

public:
    ThreadStrategy() : queue{std::make_unique<ThreadQueue>()}, m_state{new StateNormal{this}}
    {
        std::thread thread{&ThreadStrategy::Handle, this};
        thread.detach();
        while (!queue)
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }

    ~ThreadStrategy()
    {
        if (m_state != nullptr)
            delete m_state;
    }

    auto GetQueue()
    {
        return queue.get();
    }

    void Handle()
    {
        IoC::Resolve<std::shared_ptr<IoC::Register>>(
            "IoC.Register", "ThreadStrategy"sv,
            IoC::Factory{[this]([[maybe_unused]] const std::vector<std::any> &args)
                         {
                             return this;
                         }})
            ->Execute();

        while (m_state->Handle())
        {
        }

        IoC::Resolve<std::shared_ptr<IoC::Unregister>>(
            "IoC.Unregister", "ThreadStrategy"sv)
            ->Execute();
    }

    void ChangeState(State *newState)
    {
        if (m_state != nullptr)
            delete m_state;
        m_state = newState;
    }

    void HardStop()
    {
        ChangeState(new StateHardStop{this});
    }

    void SoftStop()
    {
        softStopQueueSize = queue->GetSize();
        ChangeState(new StateSoftStop{this});
    }

    void MoveTo(std::shared_ptr<IQueue<ICommand>> queue)
    {
        queueForMove = std::move(queue);
        ChangeState(new StateMoveTo{this});
    }

    void RunNormal()
    {
        ChangeState(new StateNormal{this});
    }
};

void ThreadHardStop::Execute()
{
    auto strategy = IoC::Resolve<ThreadStrategy *>("ThreadStrategy", nullptr);
    strategy->HardStop();
}

void ThreadSoftStop::Execute()
{
    auto strategy = IoC::Resolve<ThreadStrategy *>("ThreadStrategy", nullptr);
    strategy->SoftStop();
}

void ThreadMoveTo::Execute()
{
    auto strategy = IoC::Resolve<ThreadStrategy *>("ThreadStrategy", nullptr);
    strategy->MoveTo(std::move(m_queue));
}

void ThreadRun::Execute()
{
    auto strategy = IoC::Resolve<ThreadStrategy *>("ThreadStrategy", nullptr);
    strategy->RunNormal();
}
