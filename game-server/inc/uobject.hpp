#pragma once

#include <any>
#include <string>
#include <string_view>
#include <unordered_map>
#include <any>
#include <memory>

class IUObject
{
public:
    virtual ~IUObject() = default;

    virtual std::any GetProperty(std::string_view key) = 0;
    virtual void SetProperty(std::string_view key, std::any value) = 0;
};

class UObject : public IUObject
{
    std::unordered_map<std::string, std::any> map;

public:
    UObject() = default;

    [[nodiscard]] std::any GetProperty(std::string_view key) override
    {
        if (map.contains(key.data()))
        {
            return map[key.data()];
        }
        else
        {
            return nullptr;
        }
    }
    void SetProperty(std::string_view key, std::any value) override
    {
        map.insert_or_assign(key.data(), value);
    }
};
