#pragma once

#include "command.hpp"
#include <string_view>
#include <iostream>
#include <memory>

class ILogable
{
public:
    virtual ~ILogable() = default;

    virtual void Write() = 0;
};

class LogConsole : public ILogable
{
    std::string msg{};

public:
    LogConsole() = default;
    LogConsole(std::string_view msg) : msg{msg}
    {
    }
    void Write() override
    {
        std::cout << "Log: " << msg << std::endl;
    }
};

class Log : public ICommand
{
    ILogable *l;

public:
    Log(ILogable *l) : l{l}
    {
    }
    void Execute() override
    {
        l->Write();
    }
};
