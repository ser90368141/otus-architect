#pragma once

#include <typeinfo>
#include <typeindex>

#include "IUObject.hpp"
#include "command.hpp"
#include "ioc.hpp"

#include "fuel_burn.hpp"
#include "fuel_check.hpp"
#include "move.hpp"
#include "rotate.hpp"
#include "velocity_change.hpp"

class AutoGen_FuelBurnableAdapter : public IFuelBurnable
{
    IUObject *obj;

public:
    AutoGen_FuelBurnableAdapter(IUObject *obj): obj{obj} {}
    
    int GetConsumption() override
    {
        return IoC::Resolve<int>("IMovable:Consumption.get", obj);
    }
    int GetLevel() override
    {
        return IoC::Resolve<int>("IMovable:Level.get", obj);
    }

    void SetLevel(int level) override
    {
        IoC::Resolve("IMovable:Level.set", obj, level);
    }
};

class AutoGen_FuelCheckableAdapter : public IFuelCheckable
{
    IUObject *obj;

public:
    AutoGen_FuelCheckableAdapter(IUObject *obj): obj{obj} {}
    
    int GetConsumption() override
    {
        return IoC::Resolve<int>("IMovable:Consumption.get", obj);
    }
    int GetLevel() override
    {
        return IoC::Resolve<int>("IMovable:Level.get", obj);
    }

};

class AutoGen_MovableAdapter : public IMovable
{
    IUObject *obj;

public:
    AutoGen_MovableAdapter(IUObject *obj): obj{obj} {}
    
    MoveData GetPosition() override
    {
        return IoC::Resolve<MoveData>("IMovable:Position.get", obj);
    }
    MoveData GetVelocity() override
    {
        return IoC::Resolve<MoveData>("IMovable:Velocity.get", obj);
    }

    void SetPosition(MoveData position) override
    {
        IoC::Resolve("IMovable:Position.set", obj, position);
    }
};

class AutoGen_RotatableAdapter : public IRotatable
{
    IUObject *obj;

public:
    AutoGen_RotatableAdapter(IUObject *obj): obj{obj} {}
    
    int GetDirection() override
    {
        return IoC::Resolve<int>("IMovable:Direction.get", obj);
    }
    int GetAngularVelocity() override
    {
        return IoC::Resolve<int>("IMovable:AngularVelocity.get", obj);
    }
    int GetDirectionsNumber() override
    {
        return IoC::Resolve<int>("IMovable:DirectionsNumber.get", obj);
    }

    void SetDirection(int direction) override
    {
        IoC::Resolve("IMovable:Direction.set", obj, direction);
    }
};

class AutoGen_VelocityChangeableAdapter : public IVelocityChangeable
{
    IUObject *obj;

public:
    AutoGen_VelocityChangeableAdapter(IUObject *obj): obj{obj} {}
    
    MoveData GetVelocity() override
    {
        return IoC::Resolve<MoveData>("IMovable:Velocity.get", obj);
    }
    int GetDirection() override
    {
        return IoC::Resolve<int>("IMovable:Direction.get", obj);
    }
    int GetDirectionsNumber() override
    {
        return IoC::Resolve<int>("IMovable:DirectionsNumber.get", obj);
    }

    void SetVelocity(MoveData velocity) override
    {
        IoC::Resolve("IMovable:Velocity.set", obj, velocity);
    }
};


inline void RegisterAdapter()
{
    IoC::Resolve<std::shared_ptr<IoC::Register>>(
        "IoC.Register", "Adapter",
        IoC::Factory{[](const std::vector<std::any> &args) -> std::any
                     {
                         auto type{std::any_cast<std::type_index>(args.at(0))};
                         auto obj{std::any_cast<IUObject *>(args.at(1))};
                         if (type == typeid(IFuelBurnable))
                         {
                             return std::make_shared<AutoGen_FuelBurnableAdapter>(obj);
                         }
                         if (type == typeid(IFuelCheckable))
                         {
                             return std::make_shared<AutoGen_FuelCheckableAdapter>(obj);
                         }
                         if (type == typeid(IMovable))
                         {
                             return std::make_shared<AutoGen_MovableAdapter>(obj);
                         }
                         if (type == typeid(IRotatable))
                         {
                             return std::make_shared<AutoGen_RotatableAdapter>(obj);
                         }
                         if (type == typeid(IVelocityChangeable))
                         {
                             return std::make_shared<AutoGen_VelocityChangeableAdapter>(obj);
                         }
                         return nullptr;
                     }})
        ->Execute();
}
