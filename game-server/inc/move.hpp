#pragma once

#include "command.hpp"

struct MoveData
{
    int x{};
    int y{};
    MoveData operator+(MoveData r)
    {
        return MoveData{this->x + r.x, this->y + r.y};
    }
    bool operator==(const MoveData &r) const
    {
        return this->x == r.x && this->y == r.y;
    }
};

class IMovable
{
public:
    virtual ~IMovable() = default;

    virtual MoveData GetPosition() = 0;
    virtual void SetPosition(MoveData position) = 0;
    virtual MoveData GetVelocity() = 0;
};

class Move : public ICommand
{
    IMovable *m;

public:
    Move(IMovable *m) : m{m}
    {
    }
    void Execute() override
    {
        m->SetPosition(m->GetPosition() + m->GetVelocity());
    }
};
