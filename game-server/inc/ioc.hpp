#pragma once

#include <string>
#include <string_view>
#include <any>
#include <vector>
#include <functional>
#include <memory>
#include <unordered_map>
#include <utility>
#include <exception>
#include <stdexcept>

#include "command.hpp"
#include "move.hpp"

class IocException : public std::exception
{
    std::string msg{};

public:
    explicit IocException() : msg{} {}
    explicit IocException(const std::string &what) : msg{what} {}
    explicit IocException(const char *what) : msg{what} {}
    const char *what() const noexcept override
    {
        return msg.c_str();
    }
};

class IoC
{
public:
    static inline constexpr std::string_view ROOT_SCOPE_ID{"rootScope"};

private:
    thread_local static inline std::string currentScope{ROOT_SCOPE_ID};

    static void ResolveImp(
        std::string_view name, const std::vector<std::any> &args)
    {
        constexpr std::string_view exceptionMsg{"IoC: Register does not found"};
        // Find in current scope
        if (auto scope = scopes.find(currentScope); scope != scopes.end())
        {
            if (auto factory = scope->second.find(std::string{name}); factory != scope->second.end())
            {
                factory->second(args);
                return;
            }
        }

        // Check what current scope is not root scope
        if (currentScope == ROOT_SCOPE_ID)
        {
            throw IocException{exceptionMsg.data()};
        }

        // Find in root scope
        auto scope = scopes.at(std::string{ROOT_SCOPE_ID});
        if (auto factory = scope.find(std::string{name}); factory != scope.end())
        {
            factory->second(args);
            return;
        }
        throw IocException{exceptionMsg.data()};
    }

    template <typename T>
    [[nodiscard]] static T ResolveImp(
        std::string_view name, const std::vector<std::any> &args)
    {
        constexpr std::string_view exceptionMsg{"IoC: Register does not found"};
        // Find in current scope
        if (auto scope = scopes.find(currentScope); scope != scopes.end())
        {
            if (auto factory = scope->second.find(std::string{name}); factory != scope->second.end())
            {
                return std::any_cast<T>(factory->second(args));
            }
        }

        // Check what current scope is not root scope
        if (currentScope == ROOT_SCOPE_ID)
        {
            throw IocException{exceptionMsg.data()};
        }

        // Find in root scope
        auto scope = scopes.at(std::string{ROOT_SCOPE_ID});
        if (auto factory = scope.find(std::string{name}); factory != scope.end())
        {
            return std::any_cast<T>(factory->second(args));
        }
        throw IocException{exceptionMsg.data()};
    }

public:
    using Factory =
        std::function<std::any(const std::vector<std::any> &)>;
    using IocMap = std::unordered_map<std::string, Factory>;

    class Register : public ICommand
    {
        std::string name;
        Factory factory;

    public:
        Register(std::string_view name, Factory factory) : name{name}, factory{std::move(factory)} {}
        void Execute() override
        {
            scopes.at(currentScope).emplace(name, factory);
        }
    };

    class Unregister : public ICommand
    {
        std::string name;

    public:
        Unregister(std::string_view name) : name{name} {}
        void Execute() override { scopes.at(currentScope).erase(name); }
    };

    class NewScope : public ICommand
    {
        std::string scopeId;

    public:
        NewScope(std::string_view scopeId) : scopeId{scopeId} {}
        void Execute() override
        {
            scopes.emplace(scopeId, IocMap{});
        }
    };

    class SetScope : public ICommand
    {
        std::string scopeId;

    public:
        SetScope(std::string_view scopeId) : scopeId{scopeId} {}
        void Execute() override
        {
            currentScope = scopeId;
        }
    };

    template <typename T, typename... Args>
    [[nodiscard]] static T Resolve(std::string_view name,
                                   Args &&...args)
    {
        return ResolveImp<T>(
            name, std::vector{std::make_any<Args>(std::forward<Args>(args))...});
    }

    template <typename... Args>
    static void Resolve(std::string_view name,
                        Args &&...args)
    {
        ResolveImp(
            name, std::vector{std::make_any<Args>(std::forward<Args>(args))...});
    }

    thread_local static inline std::unordered_map<std::string, IocMap> scopes = {
        {
            std::string{ROOT_SCOPE_ID},
            {
                {"IoC.Register", [](const std::vector<std::any> &args)
                 {
                     return std::make_shared<Register>(
                         std::any_cast<std::string_view>(args.at(0)),
                         std::any_cast<Factory>(args.at(1)));
                 }},
                {"IoC.Unregister", [](const std::vector<std::any> &args)
                 {
                     return std::make_shared<Unregister>(
                         std::any_cast<std::string_view>(args.at(0)));
                 }},
                {"Scope.New", [](const std::vector<std::any> &args)
                 {
                     return std::make_shared<NewScope>(
                         std::any_cast<std::string_view>(args.at(0)));
                 }},
                {"Scope.Current", [](const std::vector<std::any> &args)
                 {
                     return std::make_shared<SetScope>(
                         std::any_cast<std::string_view>(args.at(0)));
                 }},
            },
        },
    };
};
