#pragma once

#include "command.hpp"
#include "ioc.hpp"
#include "queue.hpp"
#include "uobject.hpp"

#include <nlohmann/json.hpp>
#include <exception>
#include <stdexcept>
#include <queue>
#include <memory>
#include <string>
#include <string_view>
#include <unordered_map>

class Game : public ICommand
{
public:
    static constexpr std::string_view QUEUE{"GameQueue"};
    static constexpr std::string_view OBJECTS{"GameObjects"};

    using Dictionary = std::unordered_map<int, UObject>;

    struct EventMsg
    {
        std::string commandId{};
        int objectId{};
        nlohmann::json args{};

        EventMsg(const std::string &commandId, int objectId, const nlohmann::json &args) : commandId{commandId}, objectId{objectId}, args{args} {}
    };

    class CmdQueue : public SimpleQueue<ICommand>
    {
    public:
        CmdQueue() = default;
        ~CmdQueue() = default;
    };

    class EventQueue : public SafeQueue<EventMsg>
    {
    public:
        EventQueue() = default;
        ~EventQueue() = default;
    };

private:
    const std::string gameId{};
    std::unique_ptr<CmdQueue> cmdQueue{};
    Dictionary objects{};

public:
    std::unique_ptr<EventQueue> eventQueue{};

    Game(std::string_view gameId) : gameId{gameId}, cmdQueue{std::make_unique<CmdQueue>()}, eventQueue{std::make_unique<EventQueue>()}
    {
        IoC::Resolve<std::shared_ptr<IoC::NewScope>>("Scope.New", gameId)->Execute();
        IoC::Resolve<std::shared_ptr<IoC::SetScope>>("Scope.Current", gameId)->Execute();
        IoC::Resolve<std::shared_ptr<IoC::Register>>(
            "IoC.Register", QUEUE,
            IoC::Factory{[this]([[maybe_unused]] const std::vector<std::any> &args)
                         {
                             return cmdQueue.get();
                         }})
            ->Execute();
        IoC::Resolve<std::shared_ptr<IoC::Register>>(
            "IoC.Register", OBJECTS,
            IoC::Factory{[this]([[maybe_unused]] const std::vector<std::any> &args)
                         {
                             return &objects;
                         }})
            ->Execute();
    }
    ~Game()
    {
        IoC::Resolve<std::shared_ptr<IoC::Unregister>>(
            "IoC.Unregister", OBJECTS)
            ->Execute();
        IoC::Resolve<std::shared_ptr<IoC::Unregister>>(
            "IoC.Unregister", QUEUE)
            ->Execute();
    }
    void Execute() override
    {
        auto cmd = cmdQueue->Pop();
        if (!cmd)
        {
            // TODO: generate exception???
            return;
        }

        IoC::Resolve<std::shared_ptr<IoC::SetScope>>("Scope.Current", gameId)->Execute();
        cmd->Execute();
    }
};

class InterpretCommand : public ICommand
{
    std::unique_ptr<Game::EventMsg> msg{};

    std::unique_ptr<ICommand> Interpret([[maybe_unused]] std::string_view commandId, [[maybe_unused]] const nlohmann::json &args)
    {
        // TODO: interpret cmd
        return nullptr;
    }

public:
    InterpretCommand(std::unique_ptr<Game::EventMsg> msg) : msg{std::move(msg)} {}
    void Execute() override
    {
        auto obj = IoC::Resolve<Game::Dictionary *>(Game::OBJECTS, nullptr)->at(msg->objectId);
        if (auto cmd = Interpret(msg->commandId, msg->args); cmd)
            IoC::Resolve<Game::CmdQueue *>(Game::QUEUE, nullptr)->Push(std::move(cmd));
    }
};