#pragma once

#include <exception>
#include <stdexcept>
#include <queue>
#include <memory>
#include <cstdint>

using Id = std::uint32_t;

class CommandException : public std::exception
{
    std::string msg{};

public:
    explicit CommandException() : msg{} {}
    explicit CommandException(const std::string &what) : msg{what} {}
    explicit CommandException(const char *what) : msg{what} {}
    const char *what() const noexcept override
    {
        return msg.c_str();
    }
};

class ICommand
{
public:
    virtual ~ICommand() = default;
    virtual void Execute() = 0;
};
