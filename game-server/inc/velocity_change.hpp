#pragma once

#include "command.hpp"
#include "cmath"
#include "move.hpp"

class IVelocityChangeable
{
public:
    virtual ~IVelocityChangeable() = default;

    virtual MoveData GetVelocity() = 0;
    virtual void SetVelocity(MoveData velocity) = 0;
    virtual int GetDirection() = 0;
    virtual int GetDirectionsNumber() = 0;
};

class VelocityChange : public ICommand
{
    IVelocityChangeable *m;

public:
    VelocityChange(IVelocityChangeable *m) : m{m}
    {
    }
    void Execute() override
    {
        auto velocity = m->GetVelocity();
        if (velocity == MoveData{0, 0})
        {
            return;
        }
        const auto d = m->GetDirection();
        const auto dn = m->GetDirectionsNumber();
        const auto x = velocity.x + velocity.x * std::cos((2 * M_PI * d) / dn);
        const auto y = velocity.y + velocity.y * std::sin((2 * M_PI * d) / dn);
        m->SetVelocity(MoveData{static_cast<int>(x), static_cast<int>(y)});
    }
};
