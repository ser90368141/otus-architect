#pragma once

#include <unordered_set>
#include <unordered_map>
#include <vector>
#include <cstdint>
#include <algorithm>

#include "command.hpp"
#include "macro_command.hpp"
#include "move.hpp"
#include "game.hpp"

struct Neighborhood
{
    int x{};
    int y{};
    int size{};
    std::unordered_set<Id> objects{};
};

using NeighborhoodMap = std::unordered_map<Id, Neighborhood>;

class IPositionCheckable
{
public:
    virtual ~IPositionCheckable() = default;

    virtual MoveData GetPosition() = 0;
};

class PositionCheck : public ICommand
{
    IPositionCheckable *m;
    MoveData pos{};

public:
    PositionCheck(IPositionCheckable *m, MoveData pos) : m{m}, pos{pos}
    {
    }
    void Execute() override
    {
        if (const auto pos1 = m->GetPosition(); pos1.x == pos.x && pos1.y == pos.y)
            throw CommandException("Object position collision");
    }
};

class INeighborhoodFindable
{
public:
    virtual ~INeighborhoodFindable() = default;

    virtual MoveData GetPosition(Id) = 0;
    virtual Id GetObjId() = 0;
    virtual IQueue<ICommand> *GetQueue() = 0;
};

class NeighborhoodFind : public ICommand
{
    INeighborhoodFindable *m_neighborhoodFindable;
    IPositionCheckable *m_positionCheckable;
    NeighborhoodMap *m_map;

public:
    NeighborhoodFind(INeighborhoodFindable *neighborhoodFindable, IPositionCheckable *positionCheckable,
                     NeighborhoodMap *map)
        : m_neighborhoodFindable{neighborhoodFindable}, m_positionCheckable{positionCheckable}, m_map{map}
    {
    }
    void Execute() override
    {
        // Find neighborhood
        Neighborhood *neighborhoodOld = nullptr;
        Neighborhood *neighborhoodNew = nullptr;
        const auto pos = m_positionCheckable->GetPosition();
        // auto neighborhoods{m_neighborhoodFindable->GetNeighborhoods()};
        const auto objId = m_neighborhoodFindable->GetObjId();

        for (auto &n : *m_map)
        {
            const auto addr = &n.second;
            if (neighborhoodOld && neighborhoodNew)
                break; // Neighborhoods found
            if (n.second.objects.find(objId) != n.second.objects.end())
                neighborhoodOld = &n.second; // Old neighborhood found
            if (pos.x > n.second.x && pos.y > n.second.y && pos.x < n.second.x + n.second.size && pos.y < n.second.y + n.second.size)
                neighborhoodNew = &n.second; // New neighborhood found
        }

        // Remove object from old neighborhood and add it to new neighborhood
        if (neighborhoodOld != neighborhoodNew)
        {
            // Update game objects list in neighborhoods
            if (neighborhoodOld)
                neighborhoodOld->objects.erase(objId);
            if (neighborhoodNew)
                neighborhoodNew->objects.insert(objId);
        }

        // Create macro command for checking collisions
        MacroCommand::CmdArray cmdArray{};
        for (auto &id : neighborhoodNew->objects)
        {
            if (id != objId)
                cmdArray.push_back(std::make_shared<PositionCheck>(m_positionCheckable, m_neighborhoodFindable->GetPosition(id)));
        }

        if (cmdArray.size())
            m_neighborhoodFindable->GetQueue()->Push(std::move(std::make_shared<MacroCommand>(std::move(cmdArray))));
    }
};

class CollisionCheck : public ICommand
{
private:
    std::shared_ptr<MacroCommand> macro;

public:
    CollisionCheck(INeighborhoodFindable *neighborhoodFindable,
                   IPositionCheckable *positionCheckable,
                   std::vector<NeighborhoodMap> *maps)
    {
        macro = std::make_shared<MacroCommand>(MacroCommand::CmdArray{});
        MacroCommand::CmdArray cmdArray{};
        for (auto &map : *maps)
        {
            cmdArray.push_back(std::make_shared<NeighborhoodFind>(neighborhoodFindable, positionCheckable, &map));
        }
        macro = std::make_shared<MacroCommand>(std::move(cmdArray));
    }
    void Execute() override
    {
        macro->Execute();
    }
};
