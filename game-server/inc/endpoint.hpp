#pragma once

#include "queue.hpp"
#include "ioc.hpp"
#include "command.hpp"
#include "game.hpp"

#include <exception>
#include <stdexcept>
#include <queue>
#include <memory>
#include <nlohmann/json.hpp>
#include <vector>
#include <cstdint>
#include <string>
#include <string_view>

class Endpoint
{
    std::shared_ptr<IQueue<ICommand>> threadQueue{nullptr};

public:
    struct Message
    {
        std::string gameId{};
        std::string commandId{};
        int objectId{};
        nlohmann::json args{};

        inline bool operator==(const Message &rhs) { return commandId == rhs.commandId &&
                                                            objectId == rhs.objectId &&
                                                            commandId == rhs.commandId &&
                                                            args == rhs.args; }

        NLOHMANN_DEFINE_TYPE_INTRUSIVE(Message, gameId, objectId, commandId, args);
    };

    class RouteCommand : public ICommand
    {
        Message msg;

    public:
        RouteCommand(const Message &msg) : msg{msg}
        {
        }

        void Execute() override
        {
            if (msg.gameId.empty() && msg.objectId == 0)
            {
                // Command Message
                auto threadQueue = IoC::Resolve<std::shared_ptr<IQueue<ICommand>>>("ThreadQueue", nullptr);
                auto cmd = IoC::Resolve<std::shared_ptr<ICommand>>(std::string_view{msg.commandId}, msg.args);
                threadQueue->Push(std::move(cmd));
            }
            else
            {
                // Event Message
                auto game = IoC::Resolve<std::shared_ptr<ICommand>>(msg.gameId, nullptr);
                static_cast<Game *>(game.get())->eventQueue->Push(std::make_unique<Game::EventMsg>(msg.commandId, msg.objectId, msg.args));
                // TODO: push game event interpret command
            }
        }
    };

    explicit Endpoint() : threadQueue{std::make_shared<SimpleQueue<ICommand>>()} {}
    ~Endpoint() = default;

    void Routing(nlohmann::json &jsonData)
    {
        const auto msg{jsonData.get<Message>()};
        threadQueue->Push(std::make_unique<RouteCommand>(msg));
    }
};
