#pragma once

#include <basic_controller.hpp>

#include <unordered_map>
#include <string>

#include <jwt-cpp/jwt.h>

#include <nlohmann/json.hpp>

#include <keys.hpp>

#include <endpoint.hpp>
#include <cmd_handler.hpp>

using namespace cfx;
using namespace web;
using namespace http;

class MicroserviceController : public cfx::BasicController, cfx::Controller
{
    Endpoint ep{};
public:
    MicroserviceController() : BasicController() {}
    ~MicroserviceController() {}

    inline void handleGet(http_request message) override
    {
        message.reply(status_codes::NotImplemented, responseNotImpl(methods::GET));
    }

    inline void handlePut(http_request message) override
    {
        message.reply(status_codes::NotImplemented, responseNotImpl(methods::PUT));
    }

    inline void handlePost(http_request message) override
    {
        std::cout << std::endl;
        std::cout << "method: " << message.method() << std::endl;
        std::cout << "request_uri: " << message.request_uri().to_string() << std::endl;

        const auto path = requestPath(message);
        const auto body = message.extract_json().get();
        std::cout << "Body: " << body.serialize() << std::endl;

        if (path.empty())
        {
            message.reply(status_codes::BadRequest);
            return;
        }

        if (path[0] == "cmd")
        {
            if (!body.has_field("gameId") || !body.has_field("cmdId") || !body.has_field("objId") ||
                !body.has_field("args") || !body.has_field("token"))
            {
                std::cout << "Payload error" << std::endl;
                message.reply(status_codes::BadRequest);
                return;
            }

            const auto tokenStr = body.at("token").as_string();
            std::cout << "Token: " << tokenStr << std::endl;

            auto verify = jwt::verify().allow_algorithm(jwt::algorithm::rs256(JWT_PUBLIC_KEY, "", "", "")).with_issuer("auth.server");
            auto tokenDec = jwt::decode(tokenStr);

            std::error_code ec{};
            verify.verify(tokenDec, ec);
            if (ec != jwt::error::token_verification_error::ok)
            {
                std::cout << "Token verification error: " << ec.message() << std::endl;
                message.reply(status_codes::BadRequest);
                return;
            }

            // TODO: parse token payload and make endpoint msg
            for (auto &e : tokenDec.get_payload_json())
                std::cout << e.first << " = " << e.second << std::endl;

            // Check gameId
            if (body.at("gameId").as_string() != tokenDec.get_payload_json().at("gameId").get<std::string>())
            {
                std::cout << "Game ID in token is invalid: " << body.at("gameId").as_string()
                          << " != " << tokenDec.get_payload_json().at("gameId").get<std::string>() << std::endl;
                message.reply(status_codes::BadRequest);
                return;
            }

            nlohmann::json endpointMsg = nlohmann::json::parse(body.serialize());
            endpointMsg.erase("token");

            ep.Routing(endpointMsg);

            message.reply(status_codes::OK);
            return;
        }
        message.reply(status_codes::NotFound);
    }

    inline void handlePatch(http_request message) override
    {
        message.reply(status_codes::NotImplemented, responseNotImpl(methods::PATCH));
    }

    inline void handleDelete(http_request message) override
    {
        message.reply(status_codes::NotImplemented, responseNotImpl(methods::DEL));
    }

    inline void handleHead(http_request message) override
    {
        message.reply(status_codes::NotImplemented, responseNotImpl(methods::HEAD));
    }

    inline void handleOptions(http_request message) override
    {
        message.reply(status_codes::NotImplemented, responseNotImpl(methods::OPTIONS));
    }

    inline void handleTrace(http_request message) override
    {
        message.reply(status_codes::NotImplemented, responseNotImpl(methods::TRCE));
    }

    inline void handleConnect(http_request message) override
    {
        message.reply(status_codes::NotImplemented, responseNotImpl(methods::CONNECT));
    }

    inline void handleMerge(http_request message) override
    {
        message.reply(status_codes::NotImplemented, responseNotImpl(methods::MERGE));
    }

    inline void initRestOpHandlers() override
    {
        _listener.support(methods::GET, std::bind(&MicroserviceController::handleGet, this, std::placeholders::_1));
        _listener.support(methods::PUT, std::bind(&MicroserviceController::handlePut, this, std::placeholders::_1));
        _listener.support(methods::POST, std::bind(&MicroserviceController::handlePost, this, std::placeholders::_1));
        _listener.support(methods::DEL, std::bind(&MicroserviceController::handleDelete, this, std::placeholders::_1));
        _listener.support(methods::PATCH, std::bind(&MicroserviceController::handlePatch, this, std::placeholders::_1));
        _listener.support(methods::MERGE, std::bind(&MicroserviceController::handleMerge, this, std::placeholders::_1));
    }

private:
    static json::value responseNotImpl(const http::method &method)
    {
        auto response = json::value::object();
        response["serviceName"] = json::value::string("Authentication Service");
        response["http_method"] = json::value::string(method);
        return response;
    }
};