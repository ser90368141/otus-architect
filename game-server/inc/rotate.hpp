#pragma once

#include "command.hpp"

class IRotatable
{
public:
    virtual ~IRotatable() = default;

    virtual int GetDirection() = 0;
    virtual int GetAngularVelocity() = 0;
    virtual void SetDirection(int direction) = 0;
    virtual int GetDirectionsNumber() = 0;
};

class Rotate : public ICommand
{
    IRotatable *r;

public:
    Rotate(IRotatable *r) : r{r}
    {
    }
    void Execute() override
    {
        r->SetDirection((r->GetDirection() + r->GetAngularVelocity()) % r->GetDirectionsNumber());
    }
};
