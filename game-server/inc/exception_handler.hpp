#pragma once

#include <exception>
#include <typeinfo>
#include <typeindex>
#include <unordered_map>
#include <functional>
#include <memory>

#include "command.hpp"

class IExcHandler
{
public:
    using Handler = std::function<void(std::unique_ptr<ICommand>, std::exception &)>;

    virtual ~IExcHandler() = default;
    virtual void Handle(std::unique_ptr<ICommand>, std::exception &) = 0;
    virtual void AddHandler(std::type_index, std::type_index, Handler) = 0;
};

class ExcHandler : public IExcHandler
{
    std::unordered_map<std::type_index, std::unordered_map<std::type_index, Handler>> handlers{};

public:
    ExcHandler() = default;

    void AddHandler(std::type_index cmdType, std::type_index exType, Handler handler)
    {
        handlers[cmdType][exType] = handler;
    }

    void Handle(std::unique_ptr<ICommand> cmd, std::exception &e) override
    {
        return handlers.at(std::type_index(typeid(*cmd))).at(std::type_index(typeid(e)))(std::move(cmd), e);
    }
};