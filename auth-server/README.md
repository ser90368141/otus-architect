# Authentication service

## Add user

```bash
curl -X 'POST'   'http://127.0.1.1:6502/v1/auth/api/user'   -H 'accept: */*'   -H 'Content-Type: application/json'   -d '{
  "username": "userName",
  "password": "userPass"
}'
```

## Remove user

```bash
curl -X 'DELETE'   'http://127.0.1.1:6502/v1/auth/api/user'   -H 'accept: */*'   -H 'Content-Type: application/json'   -d '{
  "username": "userName",
  "password": "userPass"
}'
```

## Add game

```bash
curl -X 'POST' \
  'http://127.0.1.1:6502/v1/auth/api/game' \
  -H 'accept: text/plain' \
  -H 'Content-Type: application/json' \
  -d '{
  "players": [
    "player1",
    "player2"
  ]
}'
```

## Get token

```bash
curl -X 'GET' \
  'http://127.0.1.1:6502/v1/auth/api/game/registration?username=userName1&password=userPass1&gameId=54321'
```
