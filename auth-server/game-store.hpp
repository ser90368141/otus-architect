#pragma once

#include <string>
#include <string_view>
#include <iostream>
#include <cstdint>
#include <vector>
#include <unordered_map>
#include <random>
#include <algorithm>

#include <keys.hpp>

#include <jwt-cpp/jwt.h>
#include <picojson/picojson.h>

class GameStore
{
    struct User
    {
        std::string password{};
    };

    struct Game
    {
        std::vector<std::string> players{};
    };

    static constexpr auto JWT_EXPIRE_TIME{std::chrono::minutes{10}};

    static inline std::mt19937_64 gen64{};
    static inline std::unordered_map<std::string, User> users;
    static inline std::unordered_map<std::int64_t, Game> games;

public:
    static inline bool AddUser(std::string_view name, std::string_view password)
    {
        if (users.contains(name.data()))
            return false;
        users.insert_or_assign(name.data(), User{password.data()});
        return true;
    }

    static inline bool UpdateUser(std::string_view name, std::string_view password)
    {
        if (!users.contains(name.data()))
            return false;
        users.insert_or_assign(name.data(), User{password.data()});
        return true;
    }

    static inline bool RemoveUser(std::string_view name)
    {
        return users.erase(name.data());
    }

    static inline void PrintUsers()
    {
        std::cout << "Users:" << std::endl;
        for (const auto &user : users)
            std::cout << user.first << ":" << user.second.password << std::endl;
    }

    static inline std::int64_t AddGame(std::vector<std::string> &players)
    {
        // Check users exist
        for (const auto &player : players)
            if (!users.contains(player))
                return 0;
        const std::int64_t gameId = gen64();
        games.insert_or_assign(gameId, Game{players});
        return gameId;
    }
    static inline bool RemoveGame(std::int64_t gameId)
    {
        return games.erase(gameId);
    }
    static inline void RemoveAllGames()
    {
        games.clear();
    }

    static inline std::string GetPlayerToken(std::string_view userName, std::string_view password, std::int64_t gameId)
    {
        // Find player
        const auto player = users.find(userName.data());
        if (player == users.end())
        {
            std::cout << "Player not found: " << userName << std::endl;
            return std::string{};
        }

        // Check player password
        if (player->second.password != password.data())
        {
            std::cout << "Player password incorrect" << std::endl;
            return std::string{};
        }

        // Find game
        const auto game = games.find(gameId);
        if (game == games.end())
        {
            std::cout << "Game not found: " << gameId << std::endl;
            return std::string{};
        }

        // Find player in game
        if (std::find(game->second.players.begin(), game->second.players.end(), userName.data()) == game->second.players.end())
        {
            std::cout << "Player not found in game: " << userName << std::endl;
            return std::string{};
        }

        const auto token = jwt::create()
                               .set_issuer("auth.server")
                               .set_type("JWT")
                               .set_id("gameReg")
                               .set_issued_at(jwt::date::clock::now())
                               .set_expires_at(jwt::date::clock::now() + std::chrono::minutes{10})
                               .set_payload_claim("user", jwt::claim{std::string{userName}})
                               .set_payload_claim("gameId", picojson::value{gameId})
                               .sign(jwt::algorithm::rs256("", JWT_PRIVATE_KEY, "", ""));

        std::cout << "token:\n"
                  << token << std::endl;

        return token;
    }
};