#include <iostream>
#include <chrono>
#include <fstream>
#include <string>
#include <string_view>

#include <keys.hpp>

#include <usr_interrupt_handler.hpp>
#include <runtime_utils.hpp>
#include "microservice.hpp"

int main(int, char **)
{
    std::cout << "Authentication server starting..." << std::endl;
    std::cout << "Private key:" << std::endl
              << JWT_PRIVATE_KEY << std::endl;

    InterruptHandler::hookSIGINT();

    MicroserviceController server;
    server.setEndpoint("http://host_auto_ip4:6502/v1/auth/api");

    try
    {
        // wait for server initialization...
        server.accept().wait();
        std::cout << "Authentication Microservice now listening for requests at: " << server.endpoint() << '\n';

        InterruptHandler::waitForUserInterrupt();

        server.shutdown().wait();
    }
    catch (std::exception &e)
    {
        std::cerr << "Microservice exception: " << e.what() << std::endl;
    }
    catch (...)
    {
        RuntimeUtils::printStackTrace();
    }

    return 0;
}
