#pragma once

#include <basic_controller.hpp>

#include <unordered_map>
#include <string>

#include "game-store.hpp"

using namespace cfx;
using namespace web;
using namespace http;

class MicroserviceController : public cfx::BasicController, cfx::Controller
{
public:
    MicroserviceController() : BasicController() {}
    ~MicroserviceController() {}

    inline void handleGet(http_request message) override
    {
        std::cout << std::endl;
        std::cout << "method: " << message.method() << std::endl;
        std::cout << "request_uri: " << message.request_uri().to_string() << std::endl;

        const auto path = requestPath(message);
        const auto params = uri::split_query(message.request_uri().query());

        if (path.empty())
        {
            message.reply(status_codes::BadRequest);
            return;
        }
        if (path[0] == "game" && path[1] == "registration")
        {
            if (!params.contains("username") || !params.contains("password") || !params.contains("gameId"))
            {
                std::cout << "Registration: bad payload" << std::endl;
                message.reply(status_codes::BadRequest);
                return;
            }

            std::cout << "User '" << params.at("username") << "' registration in game '" << params.at("gameId") << "'" << std::endl;

            const auto token = GameStore::GetPlayerToken(params.at("username"), params.at("password"), std::stol(params.at("gameId")));
            if (token.empty())
            {
                std::cout << "Getting token failed" << std::endl;
                message.reply(status_codes::BadRequest);
                return;
            }

            std::cout << "Token:" << std::endl
                      << token << std::endl;

            message.reply(status_codes::OK, token);
            return;
        }
        message.reply(status_codes::NotFound);
    }

    inline void handlePut(http_request message) override
    {
        message.reply(status_codes::NotImplemented, responseNotImpl(methods::PUT));
    }

    inline void handlePost(http_request message) override
    {
        std::cout << std::endl;
        std::cout << "method: " << message.method() << std::endl;
        std::cout << "request_uri: " << message.request_uri().to_string() << std::endl;

        const auto path = requestPath(message);
        const auto body = message.extract_json().get();
        std::cout << "Body: " << body.serialize() << std::endl;

        if (path.empty())
        {
            message.reply(status_codes::BadRequest);
            return;
        }

        if (path[0] == "user")
        {
            if (!body.has_field("username") || !body.has_field("password") ||
                !body.at("username").is_string() || !body.at("password").is_string())
            {
                message.reply(status_codes::BadRequest);
                return;
            }

            std::cout << "Add user: " << body.at("username").as_string() << " " << body.at("password").as_string() << std::endl;
            if (GameStore::AddUser(body.at("username").as_string(), body.at("password").as_string()))
            {
                GameStore::PrintUsers();
                message.reply(status_codes::OK);
                return;
            }

            std::cout << "User already exists" << std::endl;
            message.reply(status_codes::NotFound);
            return;
        }
        if (path[0] == "game")
        {
            if (!body.has_field("players") || !body.at("players").is_array())
            {
                message.reply(status_codes::BadRequest);
                return;
            }

            const auto playerArrayJson = body.at("players").as_array();
            std::vector<std::string> playerArray{};
            std::transform(playerArrayJson.cbegin(), playerArrayJson.cend(),
                           std::back_inserter(playerArray),
                           [](json::value value)
                           { return value.as_string(); });

            std::cout << "Add game: " << std::endl;
            for (const auto &player : playerArray)
                std::cout << player << std::endl;

            const auto gameId = GameStore::AddGame(playerArray);
            if (!gameId)
            {
                std::cout << "Game creating error" << std::endl;
                message.reply(status_codes::NotFound);
                return;
            }

            std::cout << "Game created: " << std::to_string(gameId) << std::endl;
            message.reply(status_codes::OK, std::to_string(gameId));
            return;
        }
        message.reply(status_codes::BadRequest);
    }

    inline void handlePatch(http_request message) override
    {
        message.reply(status_codes::NotImplemented, responseNotImpl(methods::PATCH));
    }

    inline void handleDelete(http_request message) override
    {
        std::cout << std::endl;
        std::cout << "method: " << message.method() << std::endl;
        std::cout << "request_uri: " << message.request_uri().to_string() << std::endl;

        const auto path = requestPath(message);
        const auto body = message.extract_json().get();
        std::cout << "Body: " << body.serialize() << std::endl;

        if (path.empty())
        {
            message.reply(status_codes::BadRequest);
            return;
        }

        if (path[0] == "user")
        {
            if (!body.has_field("username") || !body.has_field("password") ||
                !body.at("username").is_string() || !body.at("password").is_string())
            {
                message.reply(status_codes::BadRequest);
                return;
            }

            std::cout << "Delete user: " << body.at("username").as_string() << " " << body.at("password").as_string() << std::endl;
            if (GameStore::RemoveUser(body.at("username").as_string()))
            {
                GameStore::PrintUsers();
                message.reply(status_codes::OK);
                return;
            }
            std::cout << "User not found" << std::endl;

            message.reply(status_codes::NotFound);
            return;
        }
        message.reply(status_codes::BadRequest);
    }

    inline void handleHead(http_request message) override
    {
        message.reply(status_codes::NotImplemented, responseNotImpl(methods::HEAD));
    }

    inline void handleOptions(http_request message) override
    {
        message.reply(status_codes::NotImplemented, responseNotImpl(methods::OPTIONS));
    }

    inline void handleTrace(http_request message) override
    {
        message.reply(status_codes::NotImplemented, responseNotImpl(methods::TRCE));
    }

    inline void handleConnect(http_request message) override
    {
        message.reply(status_codes::NotImplemented, responseNotImpl(methods::CONNECT));
    }

    inline void handleMerge(http_request message) override
    {
        message.reply(status_codes::NotImplemented, responseNotImpl(methods::MERGE));
    }

    inline void initRestOpHandlers() override
    {
        _listener.support(methods::GET, std::bind(&MicroserviceController::handleGet, this, std::placeholders::_1));
        _listener.support(methods::PUT, std::bind(&MicroserviceController::handlePut, this, std::placeholders::_1));
        _listener.support(methods::POST, std::bind(&MicroserviceController::handlePost, this, std::placeholders::_1));
        _listener.support(methods::DEL, std::bind(&MicroserviceController::handleDelete, this, std::placeholders::_1));
        _listener.support(methods::PATCH, std::bind(&MicroserviceController::handlePatch, this, std::placeholders::_1));
    }

private:
    static json::value responseNotImpl(const http::method &method)
    {
        auto response = json::value::object();
        response["serviceName"] = json::value::string("Authentication Service");
        response["http_method"] = json::value::string(method);
        return response;
    }
};