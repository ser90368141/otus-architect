cmake_minimum_required(VERSION 3.20)

add_compile_options(
    -Wno-array-bounds
    -Wno-maybe-uninitialized
    -Wno-old-style-cast
)

# CppAst
add_subdirectory(cppast)

# Inja
option(INJA_USE_EMBEDDED_JSON off)
option(INJA_INSTALL off)
option(BUILD_TESTING off)
option(INJA_BUILD_TESTS off)
option(BUILD_BENCHMARK off)
option(COVERALLS off)
add_subdirectory(inja)

# jwt-cpp
option(JWT_BUILD_EXAMPLES off)
option(JWT_BUILD_TESTS off)
option(JWT_ENABLE_COVERAGE off)
option(JWT_ENABLE_FUZZING off)
add_subdirectory(jwt-cpp)